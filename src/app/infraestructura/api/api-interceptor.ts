import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {failUserNotFound, failUserNotAuthorized, failDemandNotFound} from "src/environments/environment.variables";
import {AlertService} from "../utils/alert.service";
import {MessageDTO} from 'src/app/modelo/dto/messageDTO';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(
        private alertService: AlertService
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (event.body && event.body.response) {
                        switch (event.body.response.statusCode) {
                            case failUserNotFound.statusCode:
                                this.alertService.error(event.body.response);
                                break;
                            case failUserNotAuthorized.statusCode:
                                this.alertService.error(event.body.response);
                                break;
                            default:
                                break;
                        }
                    }
                    if (event.body && event.body.codigo) {
                        switch (event.body.codigo) {
                            case failDemandNotFound.statusCode:

                                let message = new MessageDTO();
                                message.type = failDemandNotFound.type;
                                message.message = failDemandNotFound.msg;
                                message.statusCode = failDemandNotFound.statusCode;

                                this.alertService.info(message);
                                break;
                            default:
                                break;
                        }
                    }

                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                this.alertService.error(error);
                return throwError(error);
            }));
    }
}
