import {Observable} from "rxjs";
import {HttpHandlerService} from "../utils/httpHandler";
import {Injectable} from "@angular/core";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class ApiManagerService {

    constructor(
        protected _http: HttpHandlerService
    ) {
    }

    public get(endpoint: string, payload = {}, opt?: { body?: any; headers?: HttpHeaders | { [header: string]: string | string[] } }): Observable<any> {
        return this._http.get(endpoint, payload, opt);
    }

    public getList(endpoint: string, opt?: { body?: any; headers?: HttpHeaders | { [header: string]: string | string[] } }): Observable<any> {
        return this._http.get(endpoint, opt);
    }

    public  post(endpoint: string, payload = {}, opt?: { body?: any; headers?: HttpHeaders | { [header: string]: string | string[] } }): Observable<any> {
        return this._http.post(endpoint, payload, opt);
    }

    public put(endpoint: string, payload = {}, opt?: { body?: any; headers?: HttpHeaders | { [header: string]: string | string[] } }): Observable<any> {
        return this._http.put(endpoint, payload, opt);
    }

    public delete(endpoint: string, payload = {}, opt?: { body?: any; headers?: HttpHeaders | { [header: string]: string | string[] } }): Observable<any> {
        return this._http.delete(endpoint, payload, opt);
    }

}
