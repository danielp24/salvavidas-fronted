import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Message, MessageService} from "primeng/api";
import {timeComponent} from "../../../../environments/environment.variables";
import {MessageCustomeDTO} from "../../../modelo/so/messageCustomeDTO";


@Component({
    selector: 'app-messages-alert-time-fade-out',
    templateUrl: './messages-alert-time-fade-out.component.html',
    styleUrls: ['./messages-alert-time-fade-out.component.css']
})
export class MessagesAlertTimeFadeOut implements OnInit, OnChanges {

    @Input() messageCustom: MessageCustomeDTO;
    msgs: Message[] = [];

    constructor() {
    }

    ngOnInit() {

    }

    ngOnChanges() {
        if (this.messageCustom) {
            let messagePush = {
                severity: this.messageCustom.severity,
                summary: this.messageCustom.summary,
                closable: this.messageCustom.closable,
                life: this.messageCustom.life,
                detail: this.messageCustom.detail
            };
            this.msgs.push(messagePush);
            setTimeout(() => {
                let index = this.msgs.indexOf(messagePush, 0);
                if (index > -1) {
                    this.msgs.splice(index, 1);
                }
            }, this.messageCustom.life * 1000);
        }

    }


}
