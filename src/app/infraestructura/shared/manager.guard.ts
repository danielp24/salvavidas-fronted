import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {isArray, isNullOrUndefined} from "util";
import {ROUTES_PATH} from "../../app.routes.names";
import {Observable} from "rxjs/internal/Observable";


@Injectable()
export class ManagerGuard implements CanActivate {

    constructor() {
    }


    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        const userJson = sessionStorage.getItem("currentUser");
        if (!userJson)
            return false;
        const usuario = JSON.parse(userJson);
        const indexParam = next.url[0].path == 'task' ? 1 : 0;


        const path = next.url[indexParam].path;

        const routePath = Object.keys(ROUTES_PATH)
            .filter(key => path == (ROUTES_PATH[key].url || ROUTES_PATH[key]))
            .map(key => ROUTES_PATH[key])
            .find((_, index) => index === 0);

        if (isNullOrUndefined(routePath.rol) && isNullOrUndefined(routePath.roles))
            return true;

        let rolUrl;

        if (!isNullOrUndefined(routePath.rol)) {

            rolUrl = routePath.rol;
        }

        if (!isNullOrUndefined(routePath.roles)) {

            const pathParam = next.url[0].path;

            rolUrl = routePath.roles[pathParam];
        }

        return usuario.body.roles.some(rol => {

                if (isArray(rolUrl))
                    return rolUrl.includes(rol.description);

                return rol.description == rolUrl;
            }
        );
    }
}
