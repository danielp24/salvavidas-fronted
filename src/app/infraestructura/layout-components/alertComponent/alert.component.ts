import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription, from} from 'rxjs';
import {AlertService} from 'src/app/infraestructura/utils/alert.service';
import {Message} from 'primeng/components/common/api';
import {errorApiUSer, estado_NoAutorizado, failureType, statusSesion} from 'src/environments/environment.variables';
import {MessageService} from 'primeng/api';
import {LoginService} from "../../../services/login.service";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import {template} from "@angular/core/src/render3";
import {timeComponent} from "../../../../environments/environment.variables"


@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html',
    styles: [`
        :host ::ng-deep button {
            margin-right: .25em;
        }

        :host ::ng-deep .custom-toast .ui-toast-message {
            color: #ffffff;
            background: #FC466B;
            background: -webkit-linear-gradient(to right, #3F5EFB, #FC466B);
            background: linear-gradient(to right, #3F5EFB, #FC466B);
        }

        :host ::ng-deep .custom-toast .ui-toast-close-icon {
            color: #ffffff;
        }
    `]
})

export class AlertComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    private logout: Subscription;
    msgs: Message[] = [];
    closable: boolean = true;

    constructor(private router: Router, private alertService: AlertService, private messageService: MessageService, private loginService: LoginService) {
    }

    ngOnInit() {
        this.subscription = this.alertService.getMessage().subscribe(message => {
            if (!!message) {

                var msg = this.chargeMessage(message);
                if (msg.toLowerCase() == estado_NoAutorizado.noAutorizado.value.toLowerCase()) {
                    this.cerrarSesion();
                    msg = "Sesión Expirada"
                }

                /* if(msg.toLowerCase().indexOf(statusSesion.contraseñaIncorrecta.nombre.toLowerCase())){
                     this.cerrarSesion();
                     msg = "Usuario o Contraseña Incorrecto"
                 }*/
                if (message.text.error.businessStatus.indexOf(errorApiUSer.BadRequest.code)!= -1) {
                    return;
                }

                    let messagePush = {severity: message.type, summary: msg, closable: this.closable};
                    this.msgs.push(messagePush);
                    setTimeout(() => {
                        let index = this.msgs.indexOf(messagePush, 0);
                        if (index > -1) {
                            this.msgs.splice(index, 1);
                        }
                    }, timeComponent.timeFadeOutMessageComponent * 1000);
                }



        });
    }


    chargeMessage(message: any): string {

        let getNameApi = "";
        let errorType = "";
        if (!!message.text.type) {
            errorType = String(message.text.type).toUpperCase();
        } else if (!!message.type) {
            errorType = String(message.type).toUpperCase();
        }
        if (errorType == String(failureType.error).toUpperCase() && message.text.status == String(failureType.cero).toUpperCase()) {
            errorType = String(message.text.status).toUpperCase();
        }
        if (errorType == String(failureType.ok).toUpperCase() && message.text.statusText == String(failureType.ok).toUpperCase()) {
            errorType = String(message.text.error.message).toUpperCase();
        }


        switch (errorType) {
            case failureType.error :
                return message.text.error.message;
            case failureType.fail:
                return message.text.msg;
            case failureType.cero:
                if (message.text.message.indexOf("task", 0))
                    return "Error de conexión con la Api BPM";


            default:
                break;
        }


        return message.text;


    }

    cerrarSesion() {


        this.logout = this.loginService.logout()
            .pipe(first())
            .subscribe(data => {
                if (!!data)

                    this.logout.unsubscribe();
                sessionStorage.clear();

                setTimeout(() => {
                    this.router.navigate(['/login']);
                    window.location.reload();
                }, 2000);
            });
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();


    }
}
