import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Output, EventEmitter, Input} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from 'src/app/services/login.service';
import {AdminLayoutComponent} from '../adminLayout/adminLayout.component';
import {UserDTO} from 'src/app/modelo/dto/userDTO';
import {first} from "rxjs/operators";

@Component({
    selector: 'app-inline-profile',
    templateUrl: './app.profile.component.html',
    animations: [
        trigger('menu', [
            state('hidden', style({
                height: '0px'
            })),
            state('visible', style({
                height: '*'
            })),
            transition('visible => hidden', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
            transition('hidden => visible', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
        ])
    ]
})

export class AppInlineProfileComponent {

    active: boolean;
    currentUser: UserDTO;
    @Output() onSecurityRole: EventEmitter<any> = new EventEmitter();
    @Output() onProfileRole: EventEmitter<any> = new EventEmitter();
    @Output() onPrivacity: EventEmitter<any> = new EventEmitter();
    @Output() onParameterRole: EventEmitter<any> = new EventEmitter();
    @Output() onUpdateData: EventEmitter<any> = new EventEmitter();
    @Output() onAdmiPlantilla: EventEmitter<any> = new EventEmitter();
    valorRol: string;
    rolAdmin: boolean = false;
    rolUser: boolean = false;
    loader: boolean;

    constructor(
        public app: AdminLayoutComponent,
        private router: Router,
        private loginService: LoginService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);
        this.valorRol = this.loginService.currentRol();
        console.log(this.valorRol);

        if (this.valorRol == "Administrator") {
            this.rolAdmin = true;
            this.rolUser = false;
        } else {
            if (this.valorRol == "Usuario") {
                this.rolUser = true;
                this.rolAdmin = false;
            }
        }
    }

    onClick(event) {
        this.active = !this.active;
        setTimeout(() => {
            this.app.layoutMenuScrollerViewChild.moveBar();
        }, 450);
        event.preventDefault();
    }

    logout() {
        this.loader = true;
        this.loginService.logout()
            .pipe(first())
            .subscribe(data => {
                if (!!data)
                    console.log('LOGEO BIEN');
                sessionStorage.clear();
                this.router.navigate(['/login']);

            });
    }

    _onSecurityRole(event): void {
        this.onSecurityRole.emit();
        event.preventDefault();
    }

    _onParameterRole(event): void {
        this.onParameterRole.emit();
        event.preventDefault();
    }

    _onUpdateData(event): void {
        this.onUpdateData.emit();
        event.preventDefault();
    }
    _onAdmiPlantilla(event): void {
        this.onAdmiPlantilla.emit();
        event.preventDefault();
    }
    _onProfile(event): void {
        this.onProfileRole.emit();
        event.preventDefault();
    }

    _onPrivacity(event): void {
        this.onPrivacity.emit();
        event.preventDefault();
    }


}
