import { ROUTES_PATH } from "src/app/app.routes.names";
import {ADMINISTRADOR, CONSULTA, COOPERATIVA, PROFESIONAL_TESORERO, USUARIO} from "../../../../app.roles";

export  const MENU_OPTIONS = [

];

export let FOR_ROLE_OPTIONS = {};

FOR_ROLE_OPTIONS[ADMINISTRADOR] = [
    {label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url]},
    {label: 'Procesos', icon: 'work', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.processes.url]},
    {label: 'Consultar Movimientos', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Pago no Liquidado', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.enrutadorPantalla.url + '/' +ROUTES_PATH.informacionPagoNoLiquidado.url]},
];

FOR_ROLE_OPTIONS[COOPERATIVA]= [
    {label: 'Inicio', icon: 'work', routerLink: ['/'+ ROUTES_PATH.home + '/' +ROUTES_PATH.welcomeUser.url]},
    {label: 'Información Pago PSE', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.informacionPagoPSE.url]},
    {label: 'Consultar Movimientos', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaCooperativa.url]},

];

FOR_ROLE_OPTIONS[USUARIO] =[
    {label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url]},
    {label: 'Tareas', icon: 'list', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]},
    {label: 'Reasignar Tareas', icon: 'subject', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.reassignTask.url]},
];

FOR_ROLE_OPTIONS[PROFESIONAL_TESORERO]=[
    {label: 'Consultar Movimientos', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Registrar Pago', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.registrarPagoConsignacion.url]},

]
FOR_ROLE_OPTIONS[CONSULTA]=[
    {label: 'Consultar Movimientos', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.consultaFogacoop.url]},
    {label: 'Reportes', icon: 'assignment', routerLink: ['/' + ROUTES_PATH.home + '/' +ROUTES_PATH.enrutadorPantalla.url + '/' + ROUTES_PATH.reportes.url]},
]
