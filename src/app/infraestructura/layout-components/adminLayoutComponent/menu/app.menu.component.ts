import { Component, Input, OnInit } from "@angular/core";
import { AdminLayoutComponent } from "../adminLayout/adminLayout.component";
import { ROUTES_PATH } from "src/app/app.routes.names";
import {  } from "src/app/app.routes.names";
import {FOR_ROLE_OPTIONS} from "./menu.options";
import {isNullOrUndefined} from "util";
import { BasicPagoDTO } from "src/app/modelo/dto/common/domain/generic/bussines/BasicPagoDTO";
import { ConsultarDetallePagosService } from "src/app/services/apipse/consultarDetallePagos.service";
import { LiquidacionPSDService } from "src/app/services/apipse/liquidacion-PSD.service";
import {UserDTO} from 'src/app/modelo/dto/userDTO';


@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    @Input() reset: boolean;

    model: any[];

    basic = new BasicPagoDTO;

    constructor(public app: AdminLayoutComponent,
        private detallePagoService: ConsultarDetallePagosService,
        private fechaCorteService: LiquidacionPSDService
        ) {}

    ngOnInit() {

        const userJson = sessionStorage.getItem("currentUser");

        //Llamando a el get basic
        this.getBusinessParameter(JSON.parse(userJson));

        this.model = [
            //{label: 'Tablero de Proceso', icon: 'dashboard', routerLink: ['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard]}
        ];

        if(!userJson)
            return;

        const usuario = JSON.parse(userJson);

        (<any[]> usuario.body.roles).forEach(r => {

            if(!isNullOrUndefined(FOR_ROLE_OPTIONS[r.description])){

                FOR_ROLE_OPTIONS[r.description].forEach( route =>{
                    if(this.model.every(item => item.label != route.label))
                        this.model = [...this.model, route];
                });
            }
        });
    }

    private getBusinessParameter(usuario: UserDTO): void {
        //Inicio de los servicios que necesitan ejecutarse para trear los parametros de negocio
        //Get Fecha de Corte
        this.fechaCorteService.getFechaCorte().subscribe(data => {
            if (data.body.fechaCorte != null) {
                this.basic.fechaCorte = String(data.body.fechaCorte);
                sessionStorage.setItem("basic", JSON.stringify(this.basic));
            }
        });

        //Get Nit Entity

        if (usuario.body.organization != null
            && usuario.body.organization != "") {
            this.detallePagoService.getEntidadByCode(usuario.body.organization).subscribe(data => {
                if (data.body[0].nit != null) {
                    this.basic.nit = String(data.body[0].nit);
                    sessionStorage.setItem("basic", JSON.stringify(this.basic));
                }
            });
        }

    }
}
