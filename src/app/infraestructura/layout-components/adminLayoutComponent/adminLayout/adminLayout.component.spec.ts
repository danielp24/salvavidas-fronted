/* tslint:disable:no-unused-variable */
import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ScrollPanelModule } from 'primeng/primeng';
import { AppBreadcrumbComponent } from '../breadcrumb/app.breadcrumb.component';
import { BreadcrumbService } from '../breadcrumb/breadcrumb.service';
import { AppFooterComponent } from '../footer/app.footer.component';
import { AppMenuComponent } from '../menu/app.menu.component';
import { AppRightpanelComponent } from '../rightpanel/app.rightpanel.component';
import { AppInlineProfileComponent } from '../profile/app.profile.component';
import { AppTopbarComponent } from '../topbar/app.topbar.component';
import { AdminLayoutComponent } from './adminLayout.component';
import { AppSubMenuComponent } from '../menu/subMenu/app.SubMenu.Component';


describe('AdminLayoutComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ScrollPanelModule ],
        declarations: [ AdminLayoutComponent,
                AppTopbarComponent,
                AppMenuComponent,
                AppSubMenuComponent,
                AppFooterComponent,
                AppBreadcrumbComponent,
                AppInlineProfileComponent,
                AppRightpanelComponent
            ],
        providers: [BreadcrumbService]
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AdminLayoutComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
