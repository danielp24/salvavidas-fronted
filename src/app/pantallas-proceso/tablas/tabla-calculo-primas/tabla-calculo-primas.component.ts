import { Component, OnInit } from '@angular/core';
import {LiquidacionPSDService} from "../../../services/apipse/liquidacion-PSD.service";
import {LiquidacionPsdDTO} from "../../../modelo/dto/pse/LiquidacionPsdDTO";

@Component({
  selector: 'app-tabla-calculo-primas',
  templateUrl: './tabla-calculo-primas.component.html',

})
export class TablaCalculoPrimasComponent implements OnInit {


    dataLiquidacion: LiquidacionPsdDTO[] = [];
    calculo: any [];

    constructor(private tableCalculo: LiquidacionPSDService ) { }

    ngOnInit() {
        this.dataLiquidacion = this.tableCalculo.getLiquidacionPSD();
        console.log(this.dataLiquidacion);

        this.calculo = [
            {field: 'vr_total_a_pagar', header: 'Valor Prima PSD'},
            {field: 'vr_base_psd', header: 'Total Base Cálculo Prima'},
            {field: 'vr_depositos', header: 'Valor Depósitos'},
            {field: 'posicion_neta_val', header: 'Posición Neta Valor'},
            {field: 'saldo_a_favor_antes', header: 'Saldo a Favor'},
            {field: 'saldo_a_cargo', header: 'Saldo a Cargo'},
            {field: 'interes_Mora', header: 'Intereses Mora'},
            {field: 'dias_Mora', header: 'Días en Mora'},
            {field: 'tasa_Mora', header: 'Tasa Mora'},
        ];
    }


}
