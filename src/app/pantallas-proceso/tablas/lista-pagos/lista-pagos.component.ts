import { Component, OnInit } from '@angular/core';
import {RequestDetallePagoDTO} from "../../../modelo/dto/pse/RequestDetallePagoDTO";
import {ConsultarDetallePagosService} from "../../../services/apipse/consultarDetallePagos.service";

@Component({
  selector: 'app-lista-pagos',
  templateUrl: './lista-pagos.component.html',
  styleUrls: ['./lista-pagos.component.css']
})
export class ListaPagosComponent implements OnInit {
    cars: RequestDetallePagoDTO[];

    constructor(private consultarPagoService: ConsultarDetallePagosService) {
    }

    ngOnInit() {


    }
}
