import { Component, OnInit } from '@angular/core';
import {TablaFechaPrimaDTO} from "../../../modelo/dto/pse/TablaFechaPrimaDTO";
import {TableFechaPrimaService} from "../../../services/apipse/table-fecha-prima.service";

@Component({
  selector: 'app-tabla-fecha-primas',
  templateUrl: './tabla-fecha-primas.component.html'

})
export class TablaFechaPrimasComponent implements OnInit {

    tablaFechaPrima: TablaFechaPrimaDTO[] = [];
    fechPrima: any[];
    constructor(private tableFechaPrimaService: TableFechaPrimaService) { }

    ngOnInit() {

        this.fechPrima = [
            {field: 'fechaInicio', header: 'Fecha Inicio'},
            {field: 'fechaFinal', header: 'Fecha Final'},
            {field: 'tipoPrima', header: 'Tipo de Sobre/Prima'},
            {field: 'valorSobrePrima', header: 'Valor Sobreprima'},
            {field: 'porcentaje', header: 'Porcentaje'}
        ];

        this.tablaFechaPrima = this.tableFechaPrimaService.getTableFechaPrima();
        console.log(this.tablaFechaPrima);
    }

}
