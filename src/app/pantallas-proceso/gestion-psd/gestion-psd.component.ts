import {Component, OnInit, AfterViewInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {ROUTES_PATH} from "../../app.routes.names";
import {environment} from "../../../environments/environment";
import {UserDTO} from "../../modelo/dto/userDTO";
import {BasicPagoDTO} from "../../modelo/dto/common/domain/generic/bussines/BasicPagoDTO";
import {BusinessVariablesByProcesDTO} from "../../modelo/dto/common/domain/generic/bussines/businessVariablesByProcesDTO";
import {ProcessCurrentDTO} from "../../modelo/dto/common/domain/generic/bussines/ProcessCurrentDTO";
import {TaskDTO} from "../../modelo/dto/common/domain/generic/bussines/taskDTO";

@Component({
    selector: 'app-gestion-psd',
    templateUrl: './gestion-psd.component.html',
    styleUrls: ['../enrutador-pantallas/enrutador-pantallas.component.css']
})
export class GestionPSDComponent implements OnInit, AfterViewInit {

    url: SafeResourceUrl;
    businessVariables: BusinessVariablesByProcesDTO;

    constructor(private sanitizer: DomSanitizer,
                private router: Router) {
    }

    ngOnInit() {
    }

    frameURL() {
        this.businessVariables = new BusinessVariablesByProcesDTO();
        this.businessVariables.proccessCurrent = (JSON.parse(sessionStorage.getItem('proccessCurrent')) != null) ? JSON.parse(sessionStorage.getItem('proccessCurrent')) : new ProcessCurrentDTO();
        // this.businessParameter.currentUser = currentUser;
        this.businessVariables.task = (JSON.parse(sessionStorage.getItem('task')) != null) ? JSON.parse(sessionStorage.getItem('task')) : new TaskDTO();
        this.businessVariables.currentUser = (JSON.parse(sessionStorage.getItem('currentUser')) != null) ? JSON.parse(sessionStorage.getItem('currentUser')) : new UserDTO();
        console.log('businessVariables: ' + JSON.stringify(this.businessVariables));
        let url = environment.iframesPath + "GestionarpagosPSD?param=" + btoa(JSON.stringify(this.businessVariables));
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    ngAfterViewInit() {
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        eventer(messageEvent, (e) => {
            if (e.data == "redirect") {
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]);
            }
        }, false);
    }
}
