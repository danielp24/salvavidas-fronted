import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilitiesFnService } from 'src/app/infraestructura/utils/utilitiesFn';
import { DatosContactoDTO } from 'src/app/modelo/dto/datosContactoDTO';

@Component({
  selector: 'app-datosContacto',
  templateUrl: './datosContacto.component.html'
})
export class DatosContactoComponent implements OnInit {

  @Input() setEditable: boolean;
  @Input() datosContacto: DatosContactoDTO;
  @Output() enviarContacto = new EventEmitter<DatosContactoDTO>();
  formaContacto: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private fn: UtilitiesFnService
  ) { 
    this.formaContacto = this.formBuilder.group({
      email: ['', Validators.required],
      direccion: '',
      telefono: ['', Validators.required]
    });
  }

  ngOnInit() {
    !!this.setEditable ? this.formaContacto.enable() : this.formaContacto.disable();

    this.onChangeObject();
    
    this.onChageForm();
  }

  onChangeObject () {
    if (!!this.datosContacto) {
      this.fn.dtoToForm(this.formaContacto.controls, this.datosContacto);
    }
  }

  onChageForm () {
    var aux = !!this.datosContacto ? this.datosContacto : new DatosContactoDTO();
    this.formaContacto.valueChanges.subscribe( val => {
                                    this.enviarContacto.emit(this.fn.formToDto(val, aux));
                                  }, error => console.log(error)
                                  );
  }

}
