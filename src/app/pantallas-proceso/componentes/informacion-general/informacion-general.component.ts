import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilitiesFnService } from 'src/app/infraestructura/utils/utilitiesFn';
import { ParticipanteDTO } from 'src/app/modelo/dto/participanteDTO';
import { NameValuePair } from 'src/app/modelo/so/nombreValor';

@Component({
  selector: 'app-informacion-general',
  templateUrl: './informacion-general.component.html'
})

export class InformacionGeneralComponent implements OnInit {
 
  @Input() tipoParticipante: any;
  @Input() tipoId: NameValuePair[];
  @Input() tipoRelacion: NameValuePair[];
  @Input() setEditable: boolean;
  @Input() participante: ParticipanteDTO;
  @Output() enviarParticipante = new EventEmitter<ParticipanteDTO>();
  formaParticipante: FormGroup;
  tipoDocIdentSelect: NameValuePair;
  tipoRelacionSelect: NameValuePair;
  parentezcoView: boolean = true;
  
  constructor(
    private _formBuilder: FormBuilder,
    private fn: UtilitiesFnService
    ) {
  }

  ngOnInit() {     
    this.formaParticipante = this._formBuilder.group({
      primerNombre: ['', Validators.required],
      segundoNombre: '',
      primerApellido: ['', Validators.required],
      segundoApellido: '',
      edad: '',
      tipoDocIdent: ['', Validators.required],
      numeroDocIdent: ['', Validators.required],
      relacion: ['', Validators.required]
    });

    !!this.setEditable ? this.formaParticipante.enable() 
                       : this.formaParticipante.disable();

    this.parentezcoView = this.tipoParticipante.tipo == "1" ? true : false;

    this.onChangeObject();
    
    this.onChageForm();
  }

  onChangeObject () {
    if (!!this.participante) {
      this.fn.dtoToForm(this.formaParticipante.controls, this.participante);
      this.tipoDocIdentSelect = this.fn.filterItemByListNvp(this.tipoId, this.participante.tipoDocIdent);
      this.tipoRelacionSelect = this.fn.filterItemByListNvp(this.tipoRelacion, this.participante.relacion);
    }
  }

  onChageForm () {
    var aux = !!this.participante ? this.participante : new ParticipanteDTO();
    this.formaParticipante.valueChanges.subscribe( val => {
                                          this.enviarParticipante.emit(this.fn.formToDto(val, aux));
                                        }, error => console.log(error)
                                        );
  }

  selectForm = function (event, controlName) {
    if (!!event.value) 
      this.formaParticipante.controls[controlName].setValue(event.value.value);
  }

}
