import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {changedPasswordDTO} from "../../../modelo/dto/changedPasswordDTO";
import {ChangedPassService} from "../../../services/changed-pass.service";
import {Message, MessageService} from "primeng/api";
import {bodyChangedPasswordDTO} from "../../../modelo/dto/common/domain/generic/user/bodyChangePasswordDTO";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {apiUser, timeComponent, statusBussines} from "../../../../environments/environment.variables";
import {UserDTO} from "../../../modelo/dto/userDTO";
import {UsuarioService} from "../../../services/usuario.service";
import {LoginService} from "../../../services/login.service";
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import {MessageCustomeDTO} from "../../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css'],

})
export class ChangePasswordComponent implements OnInit {
    cambiar = false;
    infoPopUp: boolean = false;
    form: FormGroup;
    mensaje = "";
    currentUser: UserDTO;
    body: BodyDTO;
    loader: boolean = false;
    mensajeCambioContrasena: MessageCustomeDTO;
    mensajeConfirmacionPassword: MessageCustomeDTO;
    error: boolean;
    mensajePoliticas: string;
    noValido: boolean;
    contrasenasIguales: boolean;


    constructor(private changePassService: ChangedPassService,
                private messageService: MessageService,
                private formBuilder: FormBuilder,
                private usuarioService: UsuarioService,
                private loginService: LoginService,
                private router: Router
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit() {
        this.loader = true;
        this.mensaje = "";
        this.politicasNewPassword();
        this.body = this.currentUser.body;
        this.initForm();

    }

    initForm() {
        this.form = this.formBuilder.group({
            username: ['', Validators.nullValidator],
            oldPassword: ['', Validators.required, Validators.minLength(10)],
            newPassword: ['', Validators.required, Validators.minLength(10)],
            confirmarPassword: ['', Validators.required],
        }, {validator: this.checkIfMatchingPasswords('newPassword', 'confirmarPassword')});
    }

    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[passwordKey],
                passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value && this.form.get('confirmarPassword').value != "") {
                this.contrasenasIguales = true;
                return passwordConfirmationInput.setErrors({notEquivalent: true})

            } else {
                this.contrasenasIguales = false;
                return passwordConfirmationInput.setErrors(null);

            }
        }
    }
    initConfirmarContrasena(){
        this.form.controls['confirmarPassword'].setValue('');
    }


    cambiarPassword() {
        this.cambiar = true;
    }

    closePopup() {
        this.cambiar = false;
        this.cerrarFormularioCambioContraseña();
    }

    cerrarPopUpInfo() {
        this.infoPopUp = false;

    }

    validarCaracteres() {

        this.mensaje = "";
        let cadena = this.form.get('newPassword').value;
        for (var i = 0; i < cadena.length; i++) {
            var caracter = cadena.charAt(i);

            if (caracter == "ñ" || caracter == "Ñ" ||
                caracter == "á" || caracter == "Á" ||
                caracter == "á" || caracter == "Á" ||
                caracter == "é" || caracter == "É" ||
                caracter == "í" || caracter == "Í" ||
                caracter == "ó" || caracter == "Ó" ||
                caracter == "ú" || caracter == "Ú") {
                this.mensaje = "Caracter " + cadena.charAt(i) + " no permitido";
                this.error = true;
                return this.noValido = true;
            } else {
                return this.noValido = false;
                this.error = false;

            }
        }
    }

    cambiarService() {

        if (this.form.invalid || this.noValido == true || this.contrasenasIguales == true) {
            this.initForm();
            this.form.controls['username'].setValue(this.body.username);
            return;
        }


        this.loader = true;
        let change = new changedPasswordDTO();
        let header = new headerDTO();
        let body = new bodyChangedPasswordDTO();
        body.userName = this.form.get('username').value;
        body.oldPassword = this.form.get('oldPassword').value;
        body.newPassword = this.form.get('newPassword').value;
        header.apiToken = apiUser.tokenApplication;

        change.header = header;
        change.body = body;
        this.changePassService.changedPassword(change).subscribe(data => {
                this.loader = false;
                if (!!data) {

                    if (data.status == 'OK') {

                        this.mensajeConfirmacionPassword = new MessageCustomeDTO();
                        this.mensajeConfirmacionPassword.severity = 'success';
                        this.mensajeConfirmacionPassword.summary = '¡Correcto!';
                        this.mensajeConfirmacionPassword.detail = 'Se ha realizado el cambio de la contraseña, por favor ingrese nuevamente.';
                        this.mensajeConfirmacionPassword.life = timeComponent.timeFadeOutMessageComponent;
                        this.mensajeConfirmacionPassword.closable = false;
                        this.infoPopUp = true;
                    }
                    this.closePopup();
                }
                // this.closePopup();

            },
            error1 => {
                this.loader = false;

                if (error1.status == statusBussines.BAD_REQUEST.codigo) {
                    this.mensajeCambioContrasena = new MessageCustomeDTO();
                    this.mensajeCambioContrasena.severity = 'error';
                    this.mensajeCambioContrasena.summary = '¡Error!';
                    this.mensajeCambioContrasena.detail = error1.error.message;
                    this.mensajeCambioContrasena.life = timeComponent.timeFadeOutMessageComponent;
                    this.mensajeCambioContrasena.closable = true;
                }
                 if(error1.status== statusBussines.BAD_REQUEST.codigo){
                     this.mensajeCambioContrasena = new MessageCustomeDTO();
                     this.mensajeCambioContrasena.severity = 'error';
                     this.mensajeCambioContrasena.summary = '¡Error!';
                     this.mensajeCambioContrasena.detail = error1.error.message;
                     this.mensajeCambioContrasena.life = timeComponent.timeFadeOutMessageComponent;
                     this.mensajeCambioContrasena.closable = true;

                }
            });


    }

    logout() {
        this.loader = true;
        this.loginService.logout()
            .pipe(first())
            .subscribe(data => {
                if (!!data)
                    console.log('LOGEO BIEN');
                sessionStorage.clear();
                this.router.navigate(['/login']);

            });
    }

    cerrarFormularioCambioContraseña() {
        this.ngOnInit();
        this.form.controls['username'].setValue(this.body.username);

    }

    politicasNewPassword() {
        let header = new headerDTO();
        header.apiToken = apiUser.tokenApplication;
        this.changePassService.policesNewPassword(header).subscribe(data => {
            this.loader = false;
            this.mensajePoliticas = " 1.La contraseña requiere mayúscula: \n " + data.body.body.passwordRequireUppercase + ".\n"
                + " 2.Cantidad minima de caracteres: \n" + data.body.body.passwordRequiredLength + " caracteres.\n"
                + " 3.Expiración contraseña: \n" + data.body.body.passwordExpiredDays + " días.\n"
                + " 4.Requiere numero, letra o digito: \n" + data.body.body.passwordRequireNonLetterOrDigit + ".\n"
                + " 5.Requiere dígito: \n" + data.body.body.passwordRequireDigit + ".\n"
                + " 6.Requiere Minusculas: \n" + data.body.body.passwordRequireLowercase + ".\n"
                + " 7.No se permiten Caracteres tildados y letra ñ/Ñ";


        }, error1 => {
            this.loader = false;
            this.mensaje = "HA OCURRIDO UN ERROR";
        })
    }

    cerrarPoliticas() {
        this.mensaje = "";
        this.infoPopUp = false;
        this.cambiar = true;
        this.logout();
    }
}
