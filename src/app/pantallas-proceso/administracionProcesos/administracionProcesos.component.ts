import { ChangeDetectorRef, Component, OnInit, ViewRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES_PATH } from 'src/app/app.routes.names';
import { AlertService } from 'src/app/infraestructura/utils/alert.service';
import { ProcessRequestDTO } from 'src/app/modelo/dto/processRequestDTO';
import { ContenedorService } from 'src/app/services/contenedor.service';
import { ProcesoService } from 'src/app/services/proceso.service';
import { ContenedorDTO } from '../../modelo/dto/contenedorDTO';
import { ProcesoDTO } from '../../modelo/dto/procesoDTO';
import {ConfirmationService, Message} from 'primeng/primeng';
import { LoginService } from 'src/app/services/login.service';
import { successResponse } from 'src/environments/environment.variables';
import { UserBPMDTO } from 'src/app/modelo/dto/UserBPMDTO';

@Component({
  selector: 'app-administracionProcesos',
  templateUrl: './administracionProcesos.component.html'
})
export class AdministracionProcesosComponent implements OnInit {

  procesos: ProcesoDTO[];
  listaContenedor: ContenedorDTO[];
  contenedorSelect: ContenedorDTO;
  request = new ProcessRequestDTO;
  msgServicio: Message[] = [];

  constructor(
    private contenedorService: ContenedorService,
    private procesoService: ProcesoService,
    private cd: ChangeDetectorRef,
    private router: Router,
    private alertService: AlertService,
    private confirmationService: ConfirmationService,
    private loginService: LoginService
  ) {
    //Tomando usuario del Login para el request JBPM
    this.request.ownerUser = new UserBPMDTO;
    this.loginService.currentUser.subscribe(data => {
        if(data != null){
      this.request.ownerUser.user = data.body.username;
      this.request.ownerUser.password = data.body.password;
        }
    });
  }

  ngOnInit() {
    this.cargaContenedores();
  }

  detectChanges() {
    setTimeout(() => {
      if (!(this.cd as ViewRef).destroyed) {
        this.cd.detectChanges();
      }
    }, 250);
  }

  cargaContenedores () {
    this.contenedorService.consultarContenedores(this.request)
                          .subscribe( data => {
                            if (!!data)
                              this.listaContenedor = data.containers;
                          },error => this.alertService.error(error.message),
                          () => this.detectChanges()
                          );
  }

  buscarProcesos (contenedor: ContenedorDTO) {
    this.request.containerId = contenedor.containerId;
    this.procesoService.consultarProcesos(this.request)
                       .subscribe( data => {
                          if (!!data && !!data.containers){
                            data.containers.forEach(element => {
                              if (element.containerId == contenedor.containerId)
                                this.procesos = element.processes;
                            });
                          }
                        },error => this.alertService.error(error.message),
                        () => this.detectChanges()
                        );
  }

  iniciarProceso (proceso: ProcesoDTO) {
    this.request.processesId = proceso.processId;
    this.procesoService.iniciarProceso(this.request)
                       .subscribe( data => {
                          if (!!data.response && data.response.statusCode == successResponse.statusCode){
                            const aux = data.containers[0].processes;
                            for (let index = 0; index < aux.length; index++) {
                              if (aux[index].processId == proceso.processId) {
                                this.irListaTareas(aux[index]);
                                break;
                              }
                            }
                          }
                        },error => this.alertService.error(error.message),
                        () => this.detectChanges()
                        );
  }

  irListaTareas (data: ProcesoDTO) {
    this.confirmationService.confirm({
      message: 'Numero de Instacia Creado: ' + data.instance.instanceId,
      accept: () => {
        this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.task.url]);
      },
    });
  }

}
