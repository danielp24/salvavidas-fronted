import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-reasignarTareaDetalle',
    template: `
        <p-dialog header="Perfil Usuario" [(visible)]="display" modal="modal"
                  width="560" [responsive]="true" [closable]="false">

            <form class="ui-fluid" [formGroup]="formaAsignacion">

                <div class="ui-g-12">
                    <div class="ui-g form-group">
                        <div class="ui-g-6">
                            <label>Usuario Propietario Tarea</label>
                        </div>
                        <div class="ui-g-6">
                            <input formControlName="userOwner" pInputText type="text" autocomplete="off"
                                   autocorrect="off"
                                   autocapitalize="off" spellcheck="off">
                        </div>
                    </div>
                </div>

                <div class="ui-g-12">
                    <div class="ui-g form-group">
                        <div class="ui-g-6">
                            <label>Usuario Asignar Tarea</label>
                        </div>
                        <div class="ui-g-6">
                            <input formControlName="userTarget" pInputText type="text" autocomplete="off"
                                   autocorrect="off"
                                   autocapitalize="off" spellcheck="off">
                        </div>
                    </div>
                </div>

            </form>

            <p-footer>
                <button type="button" pButton icon="ui-icon-check" (click)="reasignarUsuario()"
                        label="Guardar" class="ui-button-success" [disabled]="formaAsignacion.invalid"></button>
                <button type="button" pButton icon="ui-icon-close" (click)="cerrar()"
                        label="Cancelar" class="ui-button-danger"></button>
            </p-footer>

        </p-dialog>
    `
})
export class ReasignarTareaDetalleComponent implements OnInit {

    @Input() taskActualOwner: string;
    @Output() enviarAsignacion = new EventEmitter<string>();
    display: boolean = true;
    formaAsignacion: FormGroup
    loaderGeneral: boolean;

    constructor(
        private builder: FormBuilder
    ) {
        this.formaAsignacion = this.builder.group({
            userOwner: '',
            userTarget: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.loaderGeneral = true;
        setTimeout(() => {
            this.loaderGeneral = false;
        }, 1000);
        this.formaAsignacion.controls['userOwner'].setValue(this.taskActualOwner);
        this.formaAsignacion.controls['userOwner'].disable();
    }

    reasignarUsuario() {
        this.display = false;
        this.enviarAsignacion.emit(this.formaAsignacion.get('userTarget').value);
    }

    cerrar() {
        this.display = false;
        this.taskActualOwner = null;
        this.enviarAsignacion.emit(null);
    }

}
