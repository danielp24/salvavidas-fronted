import {ChangeDetectorRef, Component, OnInit, ViewRef} from '@angular/core';
import {Router} from '@angular/router';
import {ROUTES_PATH} from 'src/app/app.routes.names';
import {AlertService} from 'src/app/infraestructura/utils/alert.service';
import {ProcesoDTO} from 'src/app/modelo/dto/procesoDTO';
import {ProcessRequestDTO} from 'src/app/modelo/dto/processRequestDTO';
import {TareaDTO} from 'src/app/modelo/dto/tareaDTO';
import {LoginService} from 'src/app/services/login.service';
import {TareaService} from 'src/app/services/tarea.service';
import {failUserNotAuthorized, states, statesTaskActions} from 'src/environments/environment.variables';
import {UserBPMDTO} from 'src/app/modelo/dto/UserBPMDTO';
import {Message} from "primeng/api";

@Component({
    selector: 'app-administracionTareas',
    templateUrl: './administracionTareas.component.html'
})
export class AdministracionTareasComponent implements OnInit {

    procesoPadre: ProcesoDTO;
    tareas: TareaDTO[];
    request = new ProcessRequestDTO;
    msgServicio: Message[] = [];
    loader: boolean;
    loaderGeneral: boolean;

    constructor(
        private cd: ChangeDetectorRef,
        private tareaService: TareaService,
        private router: Router,
        private alertService: AlertService,
        private loginService: LoginService
    ) {
        //Tomando usuario del Login para el request JBPM
        this.request.ownerUser = new UserBPMDTO;
        this.loginService.currentUser.subscribe(data => {
            if(data != null){
            this.request.ownerUser.user = data.body.username;
            this.request.ownerUser.password = data.body.password;
            }
        });
    }

    ngOnInit() {
        this.loaderGeneral = true;
        setTimeout(() => {
            this.loaderGeneral = false;
        }, 1000);
        if (!!this.request.ownerUser)
            this.cargarTareasPorGrupo()
    }

    cargarTareasPorGrupo() {
        this.request.groups = new Array<string>();
        this.tareaService.consultarTareasPorGrupos(this.request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes) {
                    this.loader = false;
                    this.tareas = new Array<TareaDTO>();
                    data.containers[0].processes[0].taskList.forEach(element => {
                        if (element.taskStatus == states.ready ||
                            element.taskStatus == states.create ||
                            element.taskStatus == states.inProgress ||
                            element.taskStatus == states.reserved)
                            this.tareas.push(element);
                    });
                }
            });
    }

    detectChanges() {
        setTimeout(() => {
            if (!(this.cd as ViewRef).destroyed) {
                this.cd.detectChanges();
            }
        }, 250);
    }

    iniciarTarea(tarea: TareaDTO) {
        this.loaderGeneral = true;
        const userLogin = this.request.ownerUser.user;
        this.request.containerId = tarea.containerId;
        this.request.taskId = tarea.taskId.toString();

        if (tarea.taskStatus == states.inProgress) {
            if (!!tarea.taskActualOwner && tarea.taskActualOwner == userLogin) {
                this.loaderGeneral = false;
                sessionStorage.setItem('task', JSON.stringify(tarea));
                sessionStorage.setItem('proccessCurrent', JSON.stringify(this.request));
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + tarea.taskName.replace(/ /g, "")]);
            } else {
                this.loaderGeneral = false;
                this.alertService.error(failUserNotAuthorized);
            }
        } else {
            this.loaderGeneral = false;
            if (tarea.taskStatus == states.ready) {
                this.request.taskStatus = statesTaskActions.claimed;
                this.reclamarTarea(this.request);
                this.request.taskStatus = statesTaskActions.started;
                this.reclamarTarea(this.request);
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + tarea.taskName.replace(/ /g, "")]);
            } else if (tarea.taskStatus == states.reserved) {
                this.request.taskStatus = statesTaskActions.started;
                this.reclamarTarea(this.request);
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + tarea.taskName.replace(/ /g, "")]);
            } else {
                this.request.taskStatus = statesTaskActions.claimed;
                this.reclamarTarea(this.request);
                this.router.navigate(['/' + ROUTES_PATH.home + '/' + tarea.taskName.replace(/ /g, "")]);
            }
        }
    }

    reclamarTarea(request: ProcessRequestDTO) {
        this.tareaService.cambiarEstadoTarea(request)
            .subscribe(data => {
                if (!!data && !!data.containers && !!data.containers[0].processes
                    && !!data.containers[0].processes[0].taskList) {
                    const task = data.containers[0].processes[0].taskList[0];
                    sessionStorage.setItem('proccessCurrent', JSON.stringify(this.request));
                    sessionStorage.setItem('task', JSON.stringify(task));
                }
            });
    }

    refrescarTareas() {
        this.loader = true;
        this.cargarTareasPorGrupo();

    }

}
