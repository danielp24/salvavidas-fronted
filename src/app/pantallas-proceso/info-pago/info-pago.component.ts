import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Message} from "primeng/api";
import {RequestCreateTransactionDTO} from "../../modelo/dto/pse/RequestCreateTransactionDTO";
import {
    apiPse,
    caracterEspecial,
    estadoEntidad,
    gestionPagosPse,
    messageValorSugerido,
    statesPagos,
    table,
    timeComponent
} from "../../../environments/environment.variables";
import {environment} from "../../../environments/environment";
import {PseService} from "../../services/apipse/pse.service";
import {DptoService} from "../../services/depto.service";
import {NameValuePair} from "../../modelo/so/nombreValor";
import {PagosService} from "../../services/admonPagosPse.service";
import {RequestPagosDTO} from "../../modelo/dto/pse/RequestPagosDTO";
import {LiquidacionPSDService} from "../../services/apipse/liquidacion-PSD.service";
import {ResponseGetFechaCorteDTO} from "../../modelo/dto/pse/ResponseGetFechaCorteDTO";
import {RequestDetallePagoDTO} from "../../modelo/dto/pse/RequestDetallePagoDTO";
import {ConsultarDetallePagosService} from "../../services/apipse/consultarDetallePagos.service";
import {ResponseDetallePagosDTO} from "../../modelo/dto/pse/ResponseDetallePagosDTO";
import {RequestActualizarPagoDTO} from "../../modelo/dto/pse/RequestActualizarPagoDTO";
import {ThousandsPipe} from 'src/app/infraestructura/utils/pipes/ThousandsPipe';
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";
import * as moment from 'moment';

import {RequestDatosBasicosDTO} from "../../modelo/dto/pse/RequestDatosBasicosDTO";
import {BasicPagoDTO} from "../../modelo/dto/common/domain/generic/bussines/BasicPagoDTO";
import {ResponseGetDetalleLiquidacion} from "../../modelo/dto/ResponseGetDetalleLiquidacion";
import {ResponseGetBasesLiquidacion} from "../../modelo/dto/ResponseGetBasesLiquidacion";


@Component({
    selector: 'app-info-pago',
    templateUrl: './info-pago.component.html',
    styleUrls: ['./info-pago.component.css'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class InfoPagoComponent implements OnInit {
    detallesPago: ResponseDetallePagosDTO[];
    detalles: any;
    datos: ResponseGetFechaCorteDTO;
    parametrosNegocio: any;
    formPagoPse: FormGroup;
    ticketId: string;
    urlPago: string;
    estadoPago: string;
    deshabilitarBtnPagar: boolean = false;
    msgs: MessageCustomeDTO;
    msgsPagoRegistrado: MessageCustomeDTO;
    msgCreateTransaction: MessageCustomeDTO;
    msgsPrincipal: MessageCustomeDTO[] = [];
    msgSecundario: Message[] = [];
    requestCreatetransacion = new RequestCreateTransactionDTO();
    listaDepartamentos: NameValuePair[];
    listaMunicipios: NameValuePair[];
    selectDepartamento: NameValuePair;
    selectMunicipios: NameValuePair;
    errorValor: boolean;
    msgErrorValor: string = "";
    codDep: number;
    ticketIdConsulta: string;
    nitActual: string;
    codeCoop: string;
    municipio: boolean = true;
    pagosRealizados: boolean = false;
    current: any;
    pagosPorPse: any[] = [];
    infoUltimoPago: ResponseDetallePagosDTO;
    actualizoPago: boolean = false;
    datosFecha: ResponseGetFechaCorteDTO;
    loading: boolean = false;
    loadingTabla: boolean = true;
    rowsTabla: string = table.maxRowsPage;
    loader: boolean;
    datosBasicos: RequestDatosBasicosDTO;
    responseDetalleLiquidacion: ResponseGetDetalleLiquidacion;
    periodo: string;
    nombreEntidad: string;
    responseGetBasesLiquidacion: ResponseGetBasesLiquidacion;
    requestGetDetalleLiquidacion: BasicPagoDTO;
    saldoTotalDepositos: number;
    detallesRetransmision: ResponseGetDetalleLiquidacion;
    saldosPendiente: ResponseGetDetalleLiquidacion;
    datosBasicosInsertarPago: RequestDatosBasicosDTO;
    ocultarSeccionRetransmision: boolean = false;
    mgsValorSugeridoPagar: MessageCustomeDTO[] = [];
    msgGetTransaction: MessageCustomeDTO;
    mostrarNota: boolean;

    constructor(private getTransactionService: PseService,
                private createTransactionService: PseService,
                private deptoService: DptoService,
                private pagosService: PagosService,
                private liquidacionPSDService: LiquidacionPSDService,
                private detallePagoService: ConsultarDetallePagosService
    ) {
        this.initForm();
        this.getParametrosSessionStorage();

    }

    ngOnInit() {
        this.getbasesLiquidacion();
        // this.getPeriodo();
        this.obtenerNit();
        this.cargarDepartamentos();
        this.mensajePrincipal();
        this.mensajeSecundario()
        this.ngAfterContentInit();
        this.getTotalSaldoDepositos();
        this.getDetalleRetransmision();
        this.getSaldosPendientes();
        this.getFechaCorte();
        this.getDetalleLiquidacion();
    }

    ngAfterContentInit() {

        this.loader = false;

    }

    private initForm() {
        this.loader = true;

        this.formPagoPse = new FormGroup({
            nit: new FormControl('NIT', [
                Validators.required,
                Validators.maxLength(3),
                Validators.minLength(3)
            ]),
            nitCooperativa: new FormControl('', [
                Validators.required,
                Validators.minLength(9),
                Validators.maxLength(9)
            ]),
            codigoVerificacion: new FormControl('', []),
            departamento: new FormControl('', []),
            municipios: new FormControl('', []),
            nombreCooperativa: new FormControl('', []),
            direccionCooperativa: new FormControl('', []),
            telefonoCooperativa: new FormControl('', []),
            valorPagar: new FormControl('', [
                Validators.required,
                Validators.minLength(1)


            ]),

            correoCooperativa: new FormControl('', [
                Validators.required,
                Validators.maxLength(70),
                Validators.minLength(1)
            ])
        });
    }

    getParametrosSessionStorage() {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('basic'));
        let nitUser = jsonSessionStorage.nit;
        let fechaCorte = jsonSessionStorage.fechaCorte;
        fechaCorte = fechaCorte.replace("/", "-");
        fechaCorte = fechaCorte.replace("/", "-");
        this.requestGetDetalleLiquidacion = new BasicPagoDTO();
        this.requestGetDetalleLiquidacion.nit = nitUser;
        this.requestGetDetalleLiquidacion.fechaCorte = fechaCorte;
    }

    getFechaCorte(/*nit: string*/) {

        this.liquidacionPSDService.getFechaCorte().subscribe(data => {
            this.datos = data;
            this.datosFecha = data.body;
            this.periodo = data.body.periodo;
            this.detallePago();
        })


    }

    detallePago() {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('basic'));
        let nitUser = jsonSessionStorage.nit;
        const consultarPagos = new RequestDetallePagoDTO();
        const requestGetDetalleLiquidacion = new BasicPagoDTO();
        consultarPagos.fechaCorte = this.datosFecha.fechaCorte;
        consultarPagos.nit = nitUser;
        consultarPagos.estado = gestionPagosPse.estadolistarHistoricoPagos;
        if (consultarPagos.fechaCorte != null || consultarPagos.nit != null || consultarPagos.nit != "") {
            this.detallePagoService.consultarDetallePagosTrimestre(consultarPagos).subscribe(datos => {


                if (datos.body.length == 0 || datos.body == []) {
                    this.pagosRealizados = true;
                } else {
                    this.pagosRealizados = false;
                    this.detallesPago = datos.body;
                    this.detallesPago = this.orderPagosDateDesc(this.detallesPago);
                    this.verificarCamposPago();
                    if (!this.actualizoPago) {
                        this.obtenerTicketId();
                    }

                }
            });
        }
        this.detalles = [
            {field: 'nombreFormaPago', header: 'Forma Pago'},
            {field: 'nombreBanco', header: 'Nombre Banco'},
            {field: 'nroChequeTransac', header: 'Numero CUS'},
            {field: 'nombreDepto', header: 'Departamento'},
            {field: 'nombreMunicipio', header: 'Municipio'},
            {field: 'fechaPago', header: 'Fecha de Pago'},
            {field: 'valorPagado', header: 'Valor Pagado'},
            {field: 'nombreEstadoPago', header: 'Estado del Pago'}
        ];

        this.loadingTabla = false;
    }

    orderPagosDesc(pagos
                       :
                       any
    ):
        ResponseDetallePagosDTO[] {
        return pagos.sort((obj1, obj2) => {
            if (Number(obj1.nroChequeTransac) < Number(obj2.nroChequeTransac)) {
                return 1;
            }
            if (Number(obj1.nroChequeTransac) > Number(obj2.nroChequeTransac)) {
                return -1;
            }

            return 0;
        });
    }

    orderPagosDateDesc(pagos
                           :
                           any
    ):
        ResponseDetallePagosDTO[] {
        return pagos.sort((obj1, obj2) => {
            var a = obj1.fechaPago.split('/').join('');
            var b = obj2.fechaPago.split('/').join('');
            return a < b ? 1 : a > b ? -1 : 0;
        });
    }

    verificarCamposPago() {
        let pipe = new ThousandsPipe();
        for (var i in  this.detallesPago) {
            this.detallesPago[i].valorPagado = pipe.transform(this.detallesPago[i].valorPagado);
        }
    }

    obtenerTicketId() {


        for (var i in  this.detallesPago) {
            if (this.detallesPago.length > 0 && this.detallesPago[i].codigoFormaPago == 1) {
                this.pagosPorPse.push(this.detallesPago[i]);
            }
        }
        this.pagosPorPse = this.orderPagosDesc(this.pagosPorPse);
        if (this.pagosPorPse.length > 0) {
            this.infoUltimoPago = this.pagosPorPse[0];
            this.ticketIdConsulta = this.infoUltimoPago.idTicket;

            if (this.infoUltimoPago.codigoEstadoPago == 0 || this.infoUltimoPago.codigoEstadoPago == 1) {
                this.deshabilitarBtnPagar = true;
                this.getTransaction(this.ticketIdConsulta, 0, 7);
            }
        }


    }

    getTransaction(ticket: string, cont: number, i: number) {
        if (cont < i) {
            const datosActPago = new RequestActualizarPagoDTO();
            this.getTransactionService.getTransaction(ticket).subscribe(data => {
                datosActPago.idTicked = data.body.ticketId;
                if (data.body.ficode == "") {
                    datosActPago.banco = 0;
                } else {
                    datosActPago.banco = data.body.ficode.substring(2);
                }
                datosActPago.nroChequeTransaccion = data.body.trazabilityCode;
                datosActPago.estadoPago = data.body.tranState;
                this.estadoPago = data.body.tranState;
                if (this.estadoPago == statesPagos.CREATED.nombre) {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.CREATED.nombre;
                    this.msgs = new MessageCustomeDTO();
                    this.msgs.severity = 'warn';
                    this.msgs.summary = '¡Warn Message!';
                    this.msgs.detail = 'En este momento su factura ' + this.ticketIdConsulta + ' presenta un proceso de pago cuya transaccion se encuentra CREADA';
                    this.msgs.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgs.closable = false;
                    this.deshabilitarBtnPagar = true;
                } else if (this.estadoPago == statesPagos.PENDING.nombre) {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.PENDING.nombre;
                    this.msgs = new MessageCustomeDTO();
                    this.msgs.severity = 'warn';
                    this.msgs.summary = '¡Warn Message!';
                    this.msgs.detail = '\n En este momento su factura <' + this.ticketIdConsulta + '> presenta un proceso de pago cuya transaccion se encuentra PENDIENTE \n'
                        + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                        + datosActPago.nroChequeTransaccion + '>';
                    this.msgs.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgs.closable = false;

                    this.deshabilitarBtnPagar = true;

                } else if (this.estadoPago == statesPagos.EXPIRED.nombre) {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.EXPIRED.nombre;
                    datosActPago.nroChequeTransaccion = "N/A";
                    this.msgs = new MessageCustomeDTO();
                    this.msgs.severity = 'warn';
                    this.msgs.summary = '¡Warn Message!';
                    this.msgs.detail = '\n En este momento su factura <' + this.ticketIdConsulta + '> presenta un proceso de pago cuya transaccion se encuentra Expirado';
                    this.msgs.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgs.closable = false;

                    this.deshabilitarBtnPagar = false;

                } else if (this.estadoPago == statesPagos.OK.nombre || this.estadoPago == "") {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.OK.nombre;
                    this.msgs = new MessageCustomeDTO();
                    this.deshabilitarBtnPagar = false;
                    cont = 7;
                } else if (this.estadoPago == statesPagos.NOT_AUTHORIZED.nombre) {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.NOT_AUTHORIZED.nombre;
                    this.msgs = new MessageCustomeDTO();
                    this.msgs.severity = 'warn';
                    this.msgs.summary = '¡Warn Message!';
                    this.msgs.detail = '\n En este momento su factura <' + this.ticketIdConsulta + '> presenta un proceso de pago cuya transaccion fue RECHAZADA.\n'
                        + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                        + datosActPago.nroChequeTransaccion + '>';
                    this.msgs.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgs.closable = false;
                    this.deshabilitarBtnPagar = false;
                    cont = 7;
                } else if (this.estadoPago == statesPagos.FAILED.nombre) {
                    this.infoUltimoPago.nombreEstadoPago = statesPagos.FAILED.nombre;
                    this.msgs = new MessageCustomeDTO();
                    this.msgs.severity = 'warn';
                    this.msgs.summary = '¡Warn Message!';
                    this.msgs.detail = '\n En este momento su factura <' + this.ticketIdConsulta + '> presenta un proceso de pago cuya transaccion fue FALLIDA.\n'
                        + 'Si desea mayor información sobre el estado actual de su operación puede comunicarse a nuestras lineas de atención 666-666 \n o enviar un correo electronico a correo@gmail.com  y preguntar por el estado de la transaccion <'
                        + datosActPago.nroChequeTransaccion + '>';
                    this.msgs.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgs.closable = false;
                    this.deshabilitarBtnPagar = false;
                    cont = 7;
                } else {
                    this.deshabilitarBtnPagar = false;
                }

                if (this.estadoPago != this.infoUltimoPago.nombreEstadoPago) {
                    this.actualizarPago(datosActPago);
                    this.actualizoPago = true;
                    this.detallePago();
                    // this.deshabilitarBtnPagar = false;
                }

            }, error1 => {

                this.msgGetTransaction = new MessageCustomeDTO();
                this.msgGetTransaction.severity = 'error';
                this.msgGetTransaction.summary = '¡INCORRECTO!';
                this.msgGetTransaction.detail = error1.error.message + 'Servicios PSE, no disponibles';
                this.msgGetTransaction.life = timeComponent.timeFadeOutMessageComponent;
                this.msgGetTransaction.closable = false;

            });


        }
        setTimeout(() => {
            this.getTransaction(this.ticketIdConsulta, cont + 1, i)
        }, 180000);


    }


    cargarDepartamentos() {

        this.listaDepartamentos = new Array<NameValuePair>();
        this.deptoService.getList().subscribe(data => {
            for (var i in data.body) {
                var nvpGroup = new NameValuePair();
                nvpGroup.name = data.body[i].nombre;
                nvpGroup.value = data.body[i].depto;
                this.listaDepartamentos.push(nvpGroup);
                this.listaDepartamentos = [...this.listaDepartamentos];
            }

        });
    }

    cambiarDecision() {
        this.parametrosNegocio = this.selectDepartamento.value;
        this.municipio = false;


    }

    consultarMunicipios() {
        this.listaMunicipios = new Array<NameValuePair>();
        this.codDep = this.parametrosNegocio;
        if (this.codDep == null) {
            this.municipio = true;
        } else {
            this.municipio = false;
            this.deptoService.getMunicipios(this.codDep).subscribe(data => {
                for (var i in data.body) {
                    var nvpGroup = new NameValuePair();
                    nvpGroup.name = data.body[i].nombre;
                    nvpGroup.value = data.body[i].municipio;
                    this.listaMunicipios.push(nvpGroup);
                    this.listaMunicipios = [...this.listaMunicipios];
                }
            })
        }
    }


    mensajePrincipal() {
        this.msgsPrincipal = [];
        this.msgsPrincipal.push({
            severity: 'warn',
            summary: '¡ATENCIÓN!',
            detail: "Los pagos remitidos por PSE solo seran accedidos por personas jurídicas."
        });
    }

    mensajeSecundario() {
        this.msgSecundario = [];
        this.msgSecundario.push({
            severity: 'warn',
            summary: '¡ATENCIÓN!',
            detail: "El valor a pagar es responsabilidad de la liquidación que realizó la cooperativa"
        });

    }

    obtenerNit() {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        const codigoUser = jsonSessionStorage.body.organization;
        this.codeCoop = codigoUser;

        this.detallePagoService.getEntidadByCode(this.codeCoop).subscribe(data => {
            this.nitActual = data.body[0].nit;
            this.nombreEntidad = data.body[0].nombreEntidad;
            // this.getFechaCorte(/*this.nitActual*/);
            this.deptoService.getList().subscribe(deptos => {
                deptos.body.forEach(dep => {
                    if (data.body[0].departamento === dep.depto) {
                        this.datosBasicos.departamento = dep.nombre;
                    }
                })
            });

            this.deptoService.getMunicipios(data.body[0].departamento).subscribe(municipios => {
                municipios.body.forEach(mun => {

                    if (data.body[0].municipio === mun.municipio) {
                        this.datosBasicos.municipio = mun.nombre;
                    }
                });
            });

            this.datosBasicos = new RequestDatosBasicosDTO();
            this.datosBasicos.numeroIdentificacion = data.body[0].nit;
            if (data.body[0].digitoCheq == null || data.body[0].digitoCheq == undefined) {
                this.datosBasicos.codVerificacionNit = 0;
            } else {
                this.datosBasicos.codVerificacionNit = data.body[0].digitoCheq;
            }
            if (data.body[0].municipio == null || data.body[0].municipio == undefined) {
                this.datosBasicos.municipio = 0;
            } else {
                this.datosBasicos.municipio = data.body[0].municipio;
            }
            if (data.body[0].nombreEntidad == null || data.body[0].nombreEntidad == undefined) {
                this.datosBasicos.nombreCooperativa = "No Aplica";
            } else {
                this.datosBasicos.nombreCooperativa = data.body[0].nombreEntidad;
            }
            if (data.body[0].direccion == null || data.body[0].direccion == undefined) {
                this.datosBasicos.direccionCooperativa = "No Aplica";
            } else {
                this.datosBasicos.direccionCooperativa = data.body[0].direccion;
            }
            if (data.body[0].departamento == null || data.body[0].departamento == undefined) {
                this.datosBasicos.codeDepartamento = 0;
            } else {
                this.datosBasicos.codeDepartamento = data.body[0].departamento;
            }
            if (data.body[0].municipio == null || data.body[0].municipio == undefined) {
                this.datosBasicos.codeMunicipio = 0;
            } else {
                this.datosBasicos.codeMunicipio = data.body[0].municipio;
            }
            if (data.body[0].telefono == null || data.body[0].telefono == undefined) {
                this.datosBasicos.telefonoCooperativa = "No Aplica";
            } else {
                this.datosBasicos.telefonoCooperativa = data.body[0].telefono;
            }
            this.datosBasicos.correoCooperativa = jsonSessionStorage.body.email;

        })

    }

    actualizarPago(datosRecibidos: RequestActualizarPagoDTO) {
        this.detallePagoService.actualizarPago(datosRecibidos).subscribe(datos => {
            console.log(datos);

        });
    }


    crearRequest() {

        // requestCreatetransacion.ReferenceArray = new ReferenceArrayDTO();
        this.requestCreatetransacion.entityCode = apiPse.entityCode;
        this.requestCreatetransacion.srvCode = apiPse.srvCode;
        this.requestCreatetransacion.transValue = this.formPagoPse.get("valorPagar").value;
        const referenceArray: string[] = [];

        referenceArray[0] = (String)(this.datosBasicos.numeroIdentificacion) + (String)(this.datosBasicos.codVerificacionNit);
        referenceArray[1] = this.formPagoPse.get("nombreCooperativa").value;
        referenceArray[2] = this.formPagoPse.get("nit").value;
        referenceArray[3] = this.formPagoPse.get("direccionCooperativa").value;
        referenceArray[4] = this.formPagoPse.get("telefonoCooperativa").value;
        referenceArray[5] = this.formPagoPse.get("correoCooperativa").value;

        this.requestCreatetransacion.urlRedirect = environment.urlEnviroment + apiPse.urlRedirect;
        this.requestCreatetransacion.referenceArray = referenceArray;


    }


    createTransaction() {
        this.createTransactionService.createTransaction(this.requestCreatetransacion)
            .subscribe(data => {
                    if (data.body) {

                        this.ticketId = data.body.ticketId;
                        this.urlPago = data.body.ecollectUrl;
                        this.insertarPago();

                    }
                }, error1 => {
                    this.loader = false;
                    this.formPagoPse.get('valorPagar').setValue('');
                    this.msgCreateTransaction = new MessageCustomeDTO();
                    this.msgCreateTransaction.severity = 'error';
                    this.msgCreateTransaction.summary = '¡INCORRECTO!';
                    this.msgCreateTransaction.detail = "error creando transaccion hacia PSE. Por favor intente mas tarde.";
                    this.msgCreateTransaction.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgCreateTransaction.closable = false;
                }
            );

    }

    insertarPago() {

        const datosPago = new RequestPagosDTO();
        datosPago.nit = (String)(this.datosBasicos.numeroIdentificacion);
        datosPago.fechaCorte = (String)(this.datosFecha.fechaCorte);
        datosPago.fechaPago = moment().format('YYYY-MM-DD');
        console.log("fecha: " + datosPago.fechaPago);
        datosPago.codFormaPago = 1;
        datosPago.departamento = this.datosBasicos.codeDepartamento;
        datosPago.municipio = this.datosBasicos.codeMunicipio;
        /*  valorPago = this.formPagoPse.get('valorPagar').value;
          valorPago = valorPago.replace(/[.]/g, ',');*/
        //this.cambioPunto(valorPago);
        datosPago.vrPagado = parseFloat(this.formPagoPse.get('valorPagar').value);
        datosPago.codigoBancoDestino = parseInt(gestionPagosPse.codBancoDestino);
        datosPago.observacion = "";
        datosPago.estadoPago = parseInt(gestionPagosPse.estadoPagoInicial);
        datosPago.idTicket = this.ticketId;


        console.log(JSON.stringify(datosPago));
        this.pagosService.insertarPago(datosPago).subscribe(data => {
            if (!!data) {
                this.msgsPagoRegistrado = new MessageCustomeDTO();
                this.msgsPagoRegistrado.severity = 'success';
                this.msgsPagoRegistrado.summary = '¡Correcto!';
                this.msgsPagoRegistrado.detail = 'Pago registrado exitosamente';
                this.msgsPagoRegistrado.life = timeComponent.timeFadeOutMessageComponent;
                this.msgsPagoRegistrado.closable = false;

            } else {
                this.msgsPagoRegistrado = new MessageCustomeDTO();
                this.msgsPagoRegistrado.severity = 'ERROR';
                this.msgsPagoRegistrado.summary = '¡INCORRECTO!';
                this.msgsPagoRegistrado.detail = 'Pago no se ha registrado exitosamente';
                this.msgsPagoRegistrado.life = timeComponent.timeFadeOutMessageComponent;
                this.msgsPagoRegistrado.closable = false;
            }
            this.redireccionPortal(this.urlPago);
        })


    }

    /*   cambioPunto(valor: string) {
           for (let i = 0; i < valor.length; i++) {
               let caracter = valor.charAt(i);
               if (caracter == ".") {
                   caracter.replace(caracter, ",");
               }
           }
       }*/

    redireccionPortal(url
                          :
                          string
    ) {
        // @ts-ignore
        window.location = url;
    }

    validar() {
        let valor: string;
        var expreg = new RegExp(/^\d*$/)

        valor = this.formPagoPse.get('valorPagar').value;
        if (this.formPagoPse.get('valorPagar').value <= 0) {
            this.formPagoPse.get('valorPagar').setErrors({'Debe ser Mayor a 0': true});

            return;
        }

        for (let i = 0; i < valor.length; i++) {
            if (expreg.test(valor) && valor.length > 18) {
                this.formPagoPse.get('valorPagar').setValue(valor.slice(0, -1));
                this.errorValor = true;
                this.msgErrorValor = "Max 18 Enteros";
            } else {
                this.errorValor = false;
            }


            for (let i = 0; i < valor.length; i++) {
                if (valor.charAt(i) == '-') {
                    this.formPagoPse.get('valorPagar').setErrors({'Caracter No permitido': true});
                    this.formPagoPse.get('valorPagar').setValue('');

                    break;
                    return;
                }
                if (caracterEspecial.numeros.valor.indexOf(valor.charAt(i), 0) != -1) {
                    break;
                } else {
                    this.formPagoPse.get('valorPagar').setValue('');
                }
                return;
            }

        }
    }

    validarMenorCero() {
        let valor: string;
        valor = this.formPagoPse.get('valorPagar').value;
        if (this.formPagoPse.get('valorPagar').value <= 0) {
            this.formPagoPse.get('valorPagar').setValue('');
            return;
        }

        for (let i = 0; i < valor.length; i++) {
            if (valor.charAt(i) == '.') {
                let numDecimal = valor.substring(valor.lastIndexOf(".") + 1);
                if (numDecimal == '') {
                    this.formPagoPse.get('valorPagar').setValue('');
                    return;
                } else {

                }

            }
            if (caracterEspecial.numeros.valor.indexOf(valor.charAt(i), 0) != -1) {

            } else {
                this.formPagoPse.get('valorPagar').setValue('');
            }


        }
    }

    pagar() {

        if (this.formPagoPse.invalid) {
            return;
        }

        if (!this.deshabilitarBtnPagar) {
            this.loading = true;
            this.crearRequest();
            this.createTransaction();
            this.loader = true;

        }

    }

    getColorEstadoPago(estado
                           :
                           string, campo
                           :
                           string
    ):
        string {
        let estilo: string = "";
        if (campo == "nombreEstadoPago") {
            if (estado == statesPagos.CREATED.display
                || estado == statesPagos.EXITOSO.display
                || estado == statesPagos.NO_COMPROBANTE.display
                || estado == statesPagos.NO_LIQUIDADO.display
                || estado == statesPagos.OK.display
                || estado == statesPagos.PENDING.display
            ) {
                estilo = "approve-pay";
            } else if (estado == statesPagos.NOT_AUTHORIZED.display
                || estado == statesPagos.EXPIRED.display) {
                estilo = "rejected-pay";
            } else {
                estilo = "failed-pay"
            }
        }

        return estilo;
    }

    getDetalleLiquidacion() {
        this.liquidacionPSDService.getDetalleLiquidacion(this.requestGetDetalleLiquidacion).subscribe(data => {
            if (data.body) {
                if (data.body.estado != estadoEntidad.preliquidada.codigo) {
                    this.mostrarNota = false;
                }
                if (data.body.estado == estadoEntidad.preliquidada.codigo) {
                    this.mostrarNota = true;
                    this.mgsValorSugeridoPagar = [];
                    this.mgsValorSugeridoPagar.push({
                        severity: 'info',
                        summary: '¡Aviso!',
                        detail: messageValorSugerido.noValores.valor,
                    });


                }
                this.responseDetalleLiquidacion = new ResponseGetDetalleLiquidacion();
                this.responseDetalleLiquidacion.prima = data.body.prima;
                this.responseDetalleLiquidacion.primaPosNeta = data.body.primaPosNeta;
                this.responseDetalleLiquidacion.sobrePrima = data.body.sobrePrima;
                this.responseDetalleLiquidacion.saldoPendiente = data.body.saldoPendiente;
                this.responseDetalleLiquidacion.interesMora = data.body.interesMora;
                this.responseDetalleLiquidacion.saldoFavor = data.body.saldoFavor;
                this.responseDetalleLiquidacion.saldoFavorAplicadoPerAnt = data.body.saldoFavorAplicadoPerAnt;
                this.responseDetalleLiquidacion.totalRetransmision = data.body.totalRetransmision;
                this.responseDetalleLiquidacion.valorTotalSugerido = data.body.valorTotalSugerido;
            }

        })

    }

    /*  getPeriodo() {
          this.liquidacionPSDService.getPeriodo().subscribe(data => {
              this.periodo = data.body.periodoActual;
          })
      }*/

    getbasesLiquidacion() {

        this.liquidacionPSDService.getBasesLiquidacion(this.requestGetDetalleLiquidacion).subscribe(data => {
            if (data.body) {
                this.responseGetBasesLiquidacion = new ResponseGetBasesLiquidacion();
                this.responseGetBasesLiquidacion.prima = data.body.prima;
                this.responseGetBasesLiquidacion.tasaPrima = data.body.tasaPrima;
                this.responseGetBasesLiquidacion.primaPosNeta = data.body.primaPosNeta;
                this.responseGetBasesLiquidacion.tasaPrimaPosNeta = data.body.tasaPrimaPosNeta;
                this.responseGetBasesLiquidacion.sobrePrima = data.body.sobrePrima;
                this.responseGetBasesLiquidacion.tasaSobrePrima = data.body.tasaSobrePrima;
                this.responseGetBasesLiquidacion.interesMora = data.body.interesMora;
                this.responseGetBasesLiquidacion.tasaInteresMora = data.body.tasaInteresMora;
                this.responseGetBasesLiquidacion.maxDiasInteresMora = data.body.maxDiasInteresMora;
            }
        })
    }

    getTotalSaldoDepositos() {
        this.liquidacionPSDService.getSaldoTotalDepositos(this.requestGetDetalleLiquidacion).subscribe(data => {
            if (data.body) {
                this.saldoTotalDepositos = data.body.saldoDepositos;
            }
        })

    }

    getDetalleRetransmision() {

        this.liquidacionPSDService.getDetalleRetransmision(this.requestGetDetalleLiquidacion).subscribe(data => {
            if (data.body == null) {
                this.ocultarSeccionRetransmision = true;
            } else {
                this.ocultarSeccionRetransmision = false;
                this.detallesRetransmision = new ResponseGetDetalleLiquidacion();
                this.detallesRetransmision.prima = data.body.prima;
                this.detallesRetransmision.primaPosNeta = data.body.primaPosNeta;
                this.detallesRetransmision.sobrePrima = data.body.sobrePrima;
                this.detallesRetransmision.interesMora = data.body.interesMora;
                this.detallesRetransmision.totalRetransmision = data.body.totalRetransmision;
            }
        })
    }

    getSaldosPendientes() {

        this.liquidacionPSDService.getSaldosPendientes(this.requestGetDetalleLiquidacion).subscribe(data => {
                if (data.body) {
                    this.saldosPendiente = new ResponseGetDetalleLiquidacion();
                    this.saldosPendiente.prima = data.body.prima;
                    this.saldosPendiente.interesMora = data.body.interesMora;
                    this.saldosPendiente.saldosPendientesAnteriores = data.body.saldosPendientesAnteriores;

                }
            }
        )
    }
}
