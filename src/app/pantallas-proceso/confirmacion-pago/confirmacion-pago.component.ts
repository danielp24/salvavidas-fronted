import {Component, OnInit} from '@angular/core';
import {ConfirmacionPagoDTO} from "../../modelo/dto/pse/ConfirmacionPagoDTO";
import {PseService} from "../../services/apipse/pse.service";
import {ROUTES_PATH} from "../../app.routes.names";
import {Router} from "@angular/router";
import {statesPagos, gestionPagosPse, timeComponent} from "../../../environments/environment.variables";
import {ConsultarDetallePagosService} from "../../services/apipse/consultarDetallePagos.service";
import {RequestDetallePagoDTO} from "../../modelo/dto/pse/RequestDetallePagoDTO";
import {ResponseGetFechaCorteDTO} from "../../modelo/dto/pse/ResponseGetFechaCorteDTO";
import {LiquidacionPSDService} from "../../services/apipse/liquidacion-PSD.service";
import {RequestActualizarPagoDTO} from "../../modelo/dto/pse/RequestActualizarPagoDTO";
import {VerificacionPagoDTO} from 'src/app/modelo/dto/pse/VerificacionPagoDTO';
import {OwnerUserPagoDTO} from 'src/app/modelo/dto/common/domain/generic/bussines/OwnerUserPagoDTO';
import {VerificarPagoService} from "../../services/verificarPago.service";
import {Message} from "primeng/api";
import {TokenDTO} from "../../modelo/dto/TokenDTO";
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-confirmacion-pago',
    templateUrl: './confirmacion-pago.component.html',
    styleUrls: ['./confirmacion-pago.component.css'],

})
export class ConfirmacionPagoComponent implements OnInit {
    codeCoop: string;
    nitActual: string;
    datos: ResponseGetFechaCorteDTO;
    value: Date;
    ticketIdConsulta: string;
    cus: string;
    ultimoPago: number;
    loader: boolean;
    msgGetTransaction: MessageCustomeDTO;
    msgsPagoRegistrado: Message[] = [];

    confirmacionPago: ConfirmacionPagoDTO = {
        entidad: '',
        NIT: '',
        fechaPago: null,
        estadoPago: '',
        numTransaccion: null,
        banco: null,
        valorPagado: null,
        descripcion: 'Azul',


    };

    constructor(private getTransactionService: PseService,
                private router: Router,
                private actualizarPagoService: ConsultarDetallePagosService,
                private fechaCorteService: LiquidacionPSDService,
                private verificarPagoService: VerificarPagoService) {

        this.obtenerNit();
        this.loader = true;

    }


    ngOnInit() {
    }

    getFechaCorte(nit: string) {
        this.fechaCorteService.getFechaCorte().subscribe(datosFecha => {
            this.datos = datosFecha.body;
            this.obtenerTicketId(this.datos, nit)
        }, error1 => {
            this.loader = false;
            this.msgGetTransaction = new MessageCustomeDTO();
            this.msgGetTransaction.severity = 'error';
            this.msgGetTransaction.summary = '¡INCORRECTO!';
            this.msgGetTransaction.detail = error1.error.message;
            this.msgGetTransaction.life = timeComponent.timeFadeOutMessageComponent;
            this.msgGetTransaction.closable = false;
        });
    }

    obtenerTicketId(datosPago: ResponseGetFechaCorteDTO, nit: string) {

        const detallesPago = new RequestDetallePagoDTO();
        detallesPago.nit = nit;
        detallesPago.fechaCorte = datosPago.fechaCorte;
        detallesPago.estado = statesPagos.CREATED.codigo;

        this.actualizarPagoService.consultarDetalleUltimoPago(detallesPago).subscribe(datos => {
            for (var i in datos.body) {
                if (datos.body.length > 0) {
                    this.ultimoPago = datos.body.length;
                    this.ticketIdConsulta = datos.body[this.ultimoPago - 1].idTicket;
                    this.getTransaction(this.ticketIdConsulta, 0, 7);
                } else {
                    this.loader = false;
                }
            }
        })
    }

    obtenerNit() {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        const codigoUser = jsonSessionStorage.body.organization;
        this.codeCoop = codigoUser;
        this.actualizarPagoService.getEntidadByCode(this.codeCoop).subscribe(data => {
            this.nitActual = data.body[0].nit;
            this.getFechaCorte(this.nitActual);
        }, error1 => {
            this.loader = false;
            this.msgGetTransaction = new MessageCustomeDTO();
            this.msgGetTransaction.severity = 'error';
            this.msgGetTransaction.summary = '¡INCORRECTO!';
            this.msgGetTransaction.detail = error1.error.message;
            this.msgGetTransaction.life = timeComponent.timeFadeOutMessageComponent;
            this.msgGetTransaction.closable = false;
        })
    }

    getTransaction(ticket: string, cont: number, i: number) {
        this.loader = true;
        if (cont < i) {
            const datosActPago = new RequestActualizarPagoDTO();
            this.getTransactionService.getTransaction(ticket).subscribe(data => {
                    this.loader = false;
                    datosActPago.idTicked = data.body.ticketId;
                    datosActPago.banco = parseInt(data.body.ficode.substring(2));
                    datosActPago.nroChequeTransaccion = data.body.trazabilityCode;
                    datosActPago.estadoPago = data.body.tranState;
                    this.confirmacionPago.banco = data.body.bankName;
                    this.confirmacionPago.numTransaccion = data.body.referenceArray[1];

                    switch (data.body.tranState) {
                        case 'OK':
                            this.confirmacionPago.estadoPago = "Aprobada";
                            break;
                        case 'PENDING':
                            this.confirmacionPago.estadoPago = "Pendiente";
                            break;
                        case 'NOT_AUTHORIZED':
                            this.confirmacionPago.estadoPago = "Rechazada";
                            break;
                        case 'FAILED':
                            this.confirmacionPago.estadoPago = "Fallida";
                            break;
                    }
                    this.confirmacionPago.fechaPago = data.body.bankProcessDate;
                    this.confirmacionPago.entidad = data.body.referenceArray[1];
                    this.confirmacionPago.numTransaccion = data.body.ticketId;
                    this.confirmacionPago.valorPagado = data.body.transValue;
                    this.cus = data.body.trazabilityCode;
                    this.confirmacionPago.NIT = this.nitActual;


                    this.actualizarPago(datosActPago);
                    if (cont == 0) {
                        //Inicia orquestado de verificacion de pagos
                        this.iniciarVerificarPago();
                    }

                }, error1 => {
                    this.loader = false;
                    this.msgGetTransaction = new MessageCustomeDTO();
                    this.msgGetTransaction.severity = 'error';
                    this.msgGetTransaction.summary = '¡INCORRECTO!';
                    this.msgGetTransaction.detail = error1.error.message + 'Servicios PSE, no disponibles';
                    this.msgGetTransaction.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgGetTransaction.closable = false;
                }
            );
            setTimeout(() => {
                this.getTransaction(this.ticketIdConsulta, cont + 1, i)
            }, 180000);
        }
    }


    iniciarVerificarPago() {

        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        const jsonBasicLocalStorgage = JSON.parse(sessionStorage.getItem('basic'));

        var rqVerificarPago = new VerificacionPagoDTO();

        //Basic
        rqVerificarPago.basic = jsonBasicLocalStorgage;
        rqVerificarPago.tokenAutenticatedBusiness = new TokenDTO();

        //OwnerUser
        const owner = new OwnerUserPagoDTO();

        owner.user = jsonSessionStorage.body.username;
        owner.password = jsonSessionStorage.body.password;
        rqVerificarPago.ownerUser = owner;

        //codigoFormaPago
        rqVerificarPago.origenPago = gestionPagosPse.CodOrigenPagoPSE;
        rqVerificarPago.tokenAutenticatedBusiness.tokenBusiness = jsonSessionStorage.header.tokenBusiness;


        this.verificarPagoService.verificarPago(rqVerificarPago).subscribe(data => {
            this.msgsPagoRegistrado = [];
            this.msgsPagoRegistrado.push({
                severity: 'success',
                summary: 'Succes Message',
                detail: 'Pago registrado exitosamente'
            });
        }, error1 => {
            this.loader = false;
            this.msgGetTransaction = new MessageCustomeDTO();
            this.msgGetTransaction.severity = 'error';
            this.msgGetTransaction.summary = '¡INCORRECTO!';
            this.msgGetTransaction.detail = error1.error.message;
            this.msgGetTransaction.life = timeComponent.timeFadeOutMessageComponent;
            this.msgGetTransaction.closable = false;
        });
    }

    actualizarPago(datosRecibidos: RequestActualizarPagoDTO) {
        this.actualizarPagoService.actualizarPago(datosRecibidos).subscribe(datos => {
        })
    }

    finalizar() {
        let numeroRoles: number = 0;
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        jsonSessionStorage.body.roles.forEach(rol => {
                let descRol = rol.description.toLowerCase();
                if (jsonSessionStorage.body.roles.length == 1 && descRol == "cooperativa") {
                    this.router.navigate(['/' + ROUTES_PATH.home]);
                    //     this.router.navigate(['/' + ROUTES_PATH.welcomeUser.url])
                } else {
                    this.router.navigate(['/' + ROUTES_PATH.home + '/' + ROUTES_PATH.dashboard.url])
                }

            }
        )


    }

}
