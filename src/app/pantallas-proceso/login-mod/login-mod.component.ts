import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {UserDTO} from 'src/app/modelo/dto/userDTO';
import {AlertService} from '../../infraestructura/utils/alert.service';
import {LoginService} from '../../services/login.service';
import {Message, MessageService} from "primeng/api";
import {BodyDTO} from "../../modelo/dto/common/domain/generic/user/bodyDTO";
import {headerDTO} from "../../modelo/dto/common/domain/generic/user/headerDTO";

import {apiUser, recaptcha, statusBussines, timeComponent} from 'src/environments/environment.variables';
import {OwnerUserDTO} from "../../modelo/dto/OwnerUserDTO";
import {ReestablecerContraseñaDTO} from "../../modelo/dto/ReestablecerContraseña.DTO";
import {TokenDTO} from "../../modelo/dto/TokenDTO";
import {recuperarContraseña} from "src/environments/environment.variables";
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";
import {RecaptchaModule} from "ng-recaptcha";


@Component({
    selector: 'app-login-mod',
    templateUrl: './login-mod.component.html',
    styles: [`
        .val {
            font-size: 70%;
            color: red;
            font-family: "Open Sans", "Helvetica Neue", sans-serif;
            margin-top: -5%;
        }
    `]
})

export class LoginModComponent implements OnInit, AfterViewInit {
    recuperar = false;
    form: FormGroup;
    formRecover: FormGroup;
    mensaje: string = "";
    mensajeEnvioCorrectoMail: MessageCustomeDTO;
    loader: boolean;
    firstOnInit: boolean = false;
    validacionRecaptcha: boolean;
    site_key: string;
    iniciarCaptcha: boolean;
    anoFooter = new Date().getFullYear();

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private loginService: LoginService,
        private alertService: AlertService,
        private messageService: MessageService
    ) {
        // redirect to home if already logged in
        if (this.loginService.currentUserValue)
            this.router.navigate(['/']);
    }

    ngOnInit() {

        this.iniciarCaptcha = true;
        this.validacionRecaptcha = false;
        this.site_key = recaptcha.site_key;
        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.formRecover = new FormGroup({
            correoElectronico: new FormControl('', [
                Validators.required,
                Validators.pattern('[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}'),
                Validators.maxLength(70),
                Validators.minLength(1)
            ])
        });

    }

    ngAfterViewInit() {
        window.ng2recaptchaloaded;
    }


    showResponse(event: any) {
        const requestReCaptcha = recaptcha.secret;
        let response: string = event.response;

        this.loginService.verificarRecaptcha(response, requestReCaptcha).subscribe((respuestaRecaptcha: any) => {
            this.validacionRecaptcha = respuestaRecaptcha.success;
            console.log(this.validacionRecaptcha);
        })
    }

    submit() {
        if (this.form.invalid) {
            return;
        }
        this.loader = true;
        const user = new UserDTO();
        const body = new BodyDTO();
        const header = new headerDTO();
        const datosValidacion = new OwnerUserDTO();
        body.username = this.form.get('username').value;
        body.password = this.form.get('password').value;
        datosValidacion.username = body.username;
        datosValidacion.password = body.password;
        header.apiToken = apiUser.tokenApplication;
        user.header = header;
        user.body = body;
        this.loginService.login(user).pipe(first()).subscribe(data => {
            if (!!data) {
                const jsonLocalStorage = JSON.parse(sessionStorage.getItem('currentUser'));
                this.loginService.autenticar(datosValidacion).subscribe(datosAutenticacion => {
                    if (!!datosAutenticacion) {
                        jsonLocalStorage.header.tokenBusiness = datosAutenticacion.body.token;
                        sessionStorage.setItem('currentUser', JSON.stringify(jsonLocalStorage));
                        this.router.navigate(['/home']);
                    }

                }, error1 => {
                    this.loader = false;
                })
            } else {
                this.loader = false;
            }
        }, error1 => {
            this.loader = false;
        })
    }

    recuperarPassword() {
        this.recuperar = true;
    }

    closePopup() {
        this.recuperar = false;
        this.ngOnInit();
    }

    validarCorreo() {
        if (this.formRecover.invalid) {
            this.firstOnInit = true;
            return;
        }
    }

    initDialogRecoveryPassword() {
        this.firstOnInit = false;
    }

    recuperarService() {
        let correoNegrita: string = "";
        this.loader = true;
        const usuarioAdmin = new OwnerUserDTO();
        const obtenerCorreo = new ReestablecerContraseñaDTO();
        obtenerCorreo.tokenAutenticatedBusiness = new TokenDTO();

        if (this.formRecover.invalid) {
            this.firstOnInit = true;
            this.loader = false;
            return;
        }
        this.firstOnInit = false;
        obtenerCorreo.email = this.formRecover.get('correoElectronico').value;
        correoNegrita = obtenerCorreo.email;
        obtenerCorreo.email = obtenerCorreo.email.toLowerCase();


        usuarioAdmin.password = recuperarContraseña.usuarioAdmin.password;
        usuarioAdmin.username = recuperarContraseña.usuarioAdmin.username;

        this.loginService.autenticar(usuarioAdmin).subscribe(datosAutenticacion => {
            if (!!datosAutenticacion) {
                obtenerCorreo.tokenAutenticatedBusiness.tokenBusiness = datosAutenticacion.body.token;
                this.loginService.notificarRestablecerCorreo(obtenerCorreo).subscribe(data => {
                    console.log(data);
                    this.mensaje = data.message;
                    this.loader = false;
                    if (data.status == 'OK') {
                        this.mensajeEnvioCorrectoMail = new MessageCustomeDTO();
                        this.mensajeEnvioCorrectoMail.severity = 'success';
                        this.mensajeEnvioCorrectoMail.summary = '¡Correcto!';
                        this.mensajeEnvioCorrectoMail.detail = 'Se ha Enviado un Link de Recuperación de Contraseña al Siguiente Correo: ' + correoNegrita.bold();
                        this.mensajeEnvioCorrectoMail.life = timeComponent.timeFadeOutMessageComponent;
                        this.mensajeEnvioCorrectoMail.closable = false;

                    }

                    this.closePopup();
                }, error1 => {
                    this.closePopup();
                    if (error1.status == statusBussines.BAD_REQUEST.codigo) {
                        this.loader = false;
                        this.mensajeEnvioCorrectoMail = new MessageCustomeDTO();
                        this.mensajeEnvioCorrectoMail.severity = 'error';
                        this.mensajeEnvioCorrectoMail.summary = '¡Lo sentimos!';
                        this.mensajeEnvioCorrectoMail.detail = 'No se pudo Enviar Link de recuperación de Contraseña al Siguiente Correo: ' + correoNegrita.bold();
                        this.mensajeEnvioCorrectoMail.life = timeComponent.timeFadeOutMessageComponent;
                        this.mensajeEnvioCorrectoMail.closable = false;
                    } else {
                        this.loader = false;
                        this.mensajeEnvioCorrectoMail = new MessageCustomeDTO();
                        this.mensajeEnvioCorrectoMail.severity = 'error';
                        this.mensajeEnvioCorrectoMail.summary = '¡Lo sentimos!';
                        this.mensajeEnvioCorrectoMail.detail = 'En este Momento No se Encuentra Disponible Este Servicio. Intente más Tarde';
                        this.mensajeEnvioCorrectoMail.life = timeComponent.timeFadeOutMessageComponent;
                        this.mensajeEnvioCorrectoMail.closable = false;
                    }
                });
            } else {
                this.loader = false;
            }
        }, error1 => {
            this.loader = false;
        })
        this.ngOnInit();

    }

}
