import {Component, OnInit} from '@angular/core';
import {cuerpoNoticia, dominioPaginaBienvenida} from "../../../environments/environment.variables";
import {ParametrosService} from "../../services/parametros.service";
import {GetListParametrosGeneralDTO} from "../../modelo/dto/getListParametrosGeneralDTO";

@Component({
    selector: 'app-welcome-user',
    templateUrl: './welcome-user.component.html',
    styleUrls: ['./welcome-user.component.css']
})
export class WelcomeUserComponent implements OnInit {
    listContents: any[] = [];
    listIdFather: any[] = [];
    listContentCarrusel: any[] = [];

    constructor(private servicioParametros: ParametrosService) {
    }

    ngOnInit() {
        this.getPlantillaBienvenida();
    }

    getPlantillaBienvenida() {
        debugger
        const dominioBienvenida = new GetListParametrosGeneralDTO();
        dominioBienvenida.idDominio = dominioPaginaBienvenida.dominio.idDominio;
        dominioBienvenida.disabled = dominioPaginaBienvenida.dominio.disabled;
        var item = {};
        this.servicioParametros.getParametrosIdDOminio(dominioBienvenida).subscribe(data => {
                if (data.body) {
                    for (let i = 0; i < data.body.length; i++) {
                        if (data.body[i].idPadre == null) {
                            this.listIdFather.push(data.body[i]);
                        } else {
                            this.listContents.push(data.body[i]);
                        }
                    }

                    for (let i = 0; i < this.listIdFather.length; i++) {
                        item['posicion'] = parseInt(this.listIdFather[i].nombre);

                        for (let j = 0; j < this.listContents.length; j++) {
                            if (this.listContents[j].nombre == cuerpoNoticia.titulo.name && this.listContents[j].idPadre == this.listIdFather[i].idParametro) {
                                item['titulo'] = this.listContents[j].valor;
                            }
                            if (this.listContents[j].nombre == cuerpoNoticia.titulo.subtitulo && this.listContents[j].idPadre == this.listIdFather[i].idParametro) {
                                item['subtitulo'] = this.listContents[j].valor;
                            }
                            if (this.listContents[j].nombre == cuerpoNoticia.titulo.cuerpoMensaje && this.listContents[j].idPadre == this.listIdFather[i].idParametro) {
                                item['cuerpoMensaje'] = this.listContents[j].valor;
                            }
                        }
                        this.listContentCarrusel.push(JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(item)))));
                    }

                    this.listContentCarrusel = this.listContentCarrusel.sort(function (a, b) {
                        var c: number = a.posicion;
                        var d: number = b.posicion;
                        return c - d;


                    });
                }
            }
        )
    }
}
