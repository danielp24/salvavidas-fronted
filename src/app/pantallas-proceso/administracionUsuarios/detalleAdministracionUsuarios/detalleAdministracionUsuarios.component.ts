import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserDTO} from '../../../modelo/dto/userDTO';
import {NameValuePair} from 'src/app/modelo/so/nombreValor';
import {LoginService} from "../../../services/login.service";

import {RolesDTO} from "../../../modelo/dto/rolesDTO";
import {BodyDTO} from "../../../modelo/dto/common/domain/generic/user/bodyDTO";
import {UpdateDTO} from "../../../modelo/dto/updateDTO";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {UpdateBody} from "../../../modelo/dto/updateBody";
import {apiUser, successResponse} from "../../../../environments/environment.variables";
import {UsuarioService} from "../../../services/usuario.service";
import {Router} from "@angular/router";
import {Message} from 'primeng/api';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-detalleAdministracionUsuarios',
    templateUrl: './detalleAdministracionUsuarios.component.html',
    styleUrls: ['./detalleAdministracionUsuarios.component.css'],
})
export class DetalleAdministracionUsuariosComponent implements OnInit {

    @Input() usuarioHijo: UserDTO;
    @Input() usuarioHijo1: BodyDTO;
    @Input() listaTipoIdent: NameValuePair[];
    @Input() listaRol: NameValuePair[];
    @Output() guardarUsuario = new EventEmitter<UserDTO>();
    @Output() cerrarPopUp = new EventEmitter<boolean>();
    display: boolean;
    rolSelect: NameValuePair[] = [];
    newUser: boolean;
    currentUser: UserDTO;
    bandera: boolean = false;
    rolrol: RolesDTO;
    rolesUsuario: string[] = [];
    listaRol1: BodyDTO;
    detallesRolUsuario: NameValuePair[] = [];
    actualizar: boolean = false;
    mensajeConfirmacion: Message[] = [];
    index: number;
    rolesToEliminar: string[] = [];
    idUserRoles: string[] = [];
    loader: boolean;
    rolesDisponibles: NameValuePair[];
    soloUnRol: boolean = false;
    formDetallUsuario: FormGroup;

    constructor(private loginService: LoginService,
                private usuarioService: UsuarioService,
                private router: Router) {
        this.loginService.currentUser.subscribe(x =>
            this.currentUser = x);


        this.display = true;


        this.newUser = true;
    }


    ngOnInit() {
        this.initForm();
        if (!!this.usuarioHijo) {
            this.rolesActualesUsuario();
            this.listaRol1 = JSON.parse(JSON.stringify(this.usuarioHijo));


            for (var rol in this.listaRol1.roles) {

                this.rolrol = JSON.parse(JSON.stringify(this.listaRol1.roles[rol]));

                this.rolesUsuario.push(this.rolrol.description);


                var nvpGroup = new NameValuePair();
                nvpGroup.name = this.rolrol.description;
                nvpGroup.value = this.rolrol.id;
                this.detallesRolUsuario.push(nvpGroup);
                this.bandera = true;
            }

            this.rolesUsuario;
            this.rolesDisponiblesUsuario();
            for (let usuarioHijoKey in this.usuarioHijo) {
                if (usuarioHijoKey == "userName" && !!usuarioHijoKey) {
                    this.newUser = false;
                }
            }

            /*  if (!!this.usuarioHijo.body.username ) {
                  this.newUser = false;
              } */
        }


    }

    initForm() {
        this.formDetallUsuario = new FormGroup({
                nombre: new FormControl(null, [Validators.required]),
                email: new FormControl(null, [Validators.required, Validators.pattern('[A-Za-z-0-9._%+-]+@[A-Za-z0-9.-]+\.')]),
                telefono: new FormControl(null, [Validators.required]),
            }
        )
    }

    rolesDisponiblesUsuario() {
        this.rolesDisponibles = this.listaRol;

        let indices: any[] = []

        let indice = 0;
        for (let rolDisponible of this.rolesDisponibles) {
            for (let detalleRolUsuario of this.detallesRolUsuario) {
                if (detalleRolUsuario.value == rolDisponible.value) {
                    indices.push(indice)
                }
            }
            indice++;
        }

        for (let i = indices.length; i > 0; i--) {
            this.rolesDisponibles.splice(indices[i - 1], 1);
        }

    }

    rolesActualesUsuario() {
        if (this.usuarioHijo) {
            let rolesUsers;
            const idUserRoles: string[] = [];

            for (var rol in this.usuarioHijo1.roles) {
                rolesUsers = JSON.parse(JSON.stringify(this.usuarioHijo1.roles[rol]));
                idUserRoles.push(rolesUsers.id)

            }
            this.rolesToEliminar = idUserRoles;
            this.idUserRoles = idUserRoles;
        }
    }

    agregarRolUsuario() {
        const idUserRoles: string[] = [];
        for (var i in this.rolSelect) {
            this.idUserRoles.push(this.rolSelect[i].value);
        }
    }

    guardarUsuarioInfo() {
        if (this.formDetallUsuario.invalid) {
            return;
        }
        this.loader = true;

        if (this.usuarioHijo1) {
            let correo = this.usuarioHijo1.email;
            correo = correo.toLowerCase();
            //   const userCO = { user: this.currentUser }
            const userCOE = new UpdateDTO();
            userCOE.header = new headerDTO;
            userCOE.body = new UpdateBody();

            userCOE.header.apiToken = apiUser.tokenApplication;
            userCOE.body.id = this.usuarioHijo1.id
            userCOE.body.username = this.usuarioHijo1.userName;
            userCOE.body.email = correo;
            userCOE.body.name = this.usuarioHijo1.name;
            userCOE.body.phoneNumber = this.usuarioHijo1.phoneNumber;
            userCOE.body.ldapGuid = null;
            userCOE.body.organization = this.usuarioHijo1.organization;


            userCOE.body.roles = this.rolesToEliminar;

            this.usuarioService.editarUsuario(userCOE)
                .subscribe(data => {
                        this.loader = false;

                        if (!!data && !!data.body) {
                            if (data.body.status == successResponse.statusCode) {
                                this.guardarUsuario.emit(this.usuarioHijo);
                                this.display = false
                            }

                        }
                        if (data == "") {
                            this.display = false;
                            this.usuarioHijo = null;
                            this.guardarUsuario.emit(this.usuarioHijo);
                        }
                    }
                    ,
                    error1 => {
                        alert("HOla");
                    }
                );
        }


    }


    cerrar() {
        this.display = false;
        this.cerrarPopUp.emit(this.display);
    }


    eliminarRoles(index) {
        this.soloUnRol = false;
        const rolesActuales = this.rolesUsuario;
        this.index = index;
        if (this.rolesToEliminar.length == 1) {
            this.soloUnRol = true;
        } else {
            this.soloUnRol = false;
            if (index > -1) {
                this.rolesToEliminar.splice(this.index, 1);
                this.rolesUsuario.splice(this.index, 1);
                this.rolesUsuario = [...rolesActuales];
            }
        }
    }
}

