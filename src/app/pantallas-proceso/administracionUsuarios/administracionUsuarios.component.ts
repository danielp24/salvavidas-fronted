import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ConfirmationService, Message} from 'primeng/primeng';
import {UtilitiesFnService} from 'src/app/infraestructura/utils/utilitiesFn';
import {NameValuePair} from 'src/app/modelo/so/nombreValor';
import {UsuarioService} from 'src/app/services/usuario.service';
import {UserDTO} from '../../modelo/dto/userDTO';
import {LoginService} from 'src/app/services/login.service';
import {RolesDTO} from "../../modelo/dto/rolesDTO";
import {apiUser, gestionCodEntidad, timeComponent} from "../../../environments/environment.variables";
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-administracionUsuarios',
    templateUrl: './administracionUsuarios.component.html'
})
export class AdministracionUsuariosComponent implements OnInit {

    msgServicio: MessageCustomeDTO;
    grupoUsuarios: UserDTO[];
    usuarioPadre: UserDTO;
    formUser: FormGroup;
    listaTipoIdent: NameValuePair[];
    listaRol: NameValuePair[];
    index: number = -1;
    currentUser: UserDTO;
    roles: RolesDTO;
    i: number;
    loginNameBusquedaToLowerCase: string;
    userNameBusquedaToLowerCase: string;
    rolNameBusquedaToLowerCase: string;
    entidadSeleccionada: boolean = true;
    listaEntidades: NameValuePair[];
    selectEntidad: NameValuePair;
    codigoEntidad: string;
    loader: boolean;
    botonBuscar: boolean = true;
    usuariosEntidad: any;
    loginName: string;
    userName: string;
    rolName: string;
    entidad: NameValuePair;
    filtroEntidades: any[];
    brand: string;
    query: string;

    constructor(
        private _formBuilder: FormBuilder,
        private fn: UtilitiesFnService,
        private confirmationService: ConfirmationService,
        private loginService: LoginService,
        private usuarioService: UsuarioService
    ) {
        this.loginService.currentUser.subscribe(x => this.currentUser = x);

        this.formUser = this._formBuilder.group({
            usuario: '',
            nombre: '',
            rol: ''
        });
    }

    ngOnInit() {
        this.currentUser;
        this.filterEntidadySelect(event);
        this.cargarGrupos();
        if (this.grupoUsuarios != null) {
            this.buscarGrupo();
        }

    }


    cargarGrupos() {
        this.loader = true;
        this.listaRol = new Array<NameValuePair>();
        this.usuarioService.consultarGrupos(this.currentUser)
            .subscribe(data => {
                this.loader = false;
                data.body.body.forEach(element => {
                    var nvpGroup = new NameValuePair();
                    nvpGroup.name = element.description;
                    nvpGroup.value = element.id;
                    this.listaRol.push(nvpGroup);

                });
            }, error => console.log(error))
        ;
    }


    buscarGrupo() {

        this.loader = true;
        let codEntidad = this.codigoEntidad;

        this.usuarioService.consultarUsuarios(this.currentUser, codEntidad)
            .subscribe(data => {
                if (Object.keys(data.body.body).length === 0) {
                    this.msgServicio = new MessageCustomeDTO();
                    this.msgServicio.severity = 'error';
                    this.msgServicio.summary = '¡Aviso!';
                    this.msgServicio.detail = apiUser.msgServiceListOrg;
                    this.msgServicio.life = timeComponent.timeFadeOutMessageComponent;
                    this.msgServicio.closable = false;

                    this.entidadSeleccionada = true;
                    this.loader = false;

                } else {
                    this.usuariosEntidad = data;
                    this.botonBuscar = true;
                    this.loader = false;
                    this.entidadSeleccionada = false;
                    this.grupoUsuarios = data.body.body;
                }
            });

    }

    organizarInfoBusqueda() {


        this.loginName = this.formUser.get("usuario").value;
        this.rolName = this.formUser.get("rol").value;
        this.userName = this.formUser.get("nombre").value;

        this.loginNameBusquedaToLowerCase = this.loginName;
        this.loginNameBusquedaToLowerCase = this.loginNameBusquedaToLowerCase.toLowerCase().trim();

        this.rolNameBusquedaToLowerCase = this.rolName;
        this.rolNameBusquedaToLowerCase = this.rolNameBusquedaToLowerCase.toLowerCase().trim();

        this.userNameBusquedaToLowerCase = this.userName;
        this.userNameBusquedaToLowerCase = this.userNameBusquedaToLowerCase.toLowerCase().trim();

        this.filtroBusqueda();

    }


    filtroBusqueda() {

        if (this.loginNameBusquedaToLowerCase != ""
            && this.rolNameBusquedaToLowerCase != ""
            && this.userNameBusquedaToLowerCase != "") {
            this.busquedaUsuarioRolNombre();

        } else if (this.loginNameBusquedaToLowerCase == ""
            && this.rolNameBusquedaToLowerCase != ""
            && this.userNameBusquedaToLowerCase != "") {
            this.busquedaUserNameAndRol();

        } else if (this.loginNameBusquedaToLowerCase != ""
            && this.rolNameBusquedaToLowerCase == ""
            && this.userNameBusquedaToLowerCase != "") {
            this.busquedaUsuarioUserName();

        } else if (this.loginNameBusquedaToLowerCase != ""
            && this.userNameBusquedaToLowerCase == ""
            && this.rolNameBusquedaToLowerCase == "") {
            this.busquedaUsuarioAndRol();

        } else if (this.loginNameBusquedaToLowerCase != ""
            && this.userNameBusquedaToLowerCase == ""
            && this.rolNameBusquedaToLowerCase != "") {
            this.busquedaUsuario();

        } else if (this.userNameBusquedaToLowerCase == ""
            && this.loginNameBusquedaToLowerCase == ""
            && this.rolNameBusquedaToLowerCase != "") {
            this.busquedaRol();

        } else if (this.loginNameBusquedaToLowerCase == ""
            && this.userNameBusquedaToLowerCase != ""
            && this.rolNameBusquedaToLowerCase == "") {
            this.busquedabyUserName();
        } else if (this.userNameBusquedaToLowerCase == ""
            && this.loginNameBusquedaToLowerCase == ""
            && this.rolNameBusquedaToLowerCase == "") {
            this.grupoUsuarios = this.usuariosEntidad.body.body;
        }


    }

    busquedaUsuarioRolNombre() {


        let usuariosByUsuario: any[] = [];
        let usuariosByUsuarioUserName: any[] = [];
        let usuariosByUsuarioUserNameRol: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {

            let loginNameToLowerCase = user.userName.toLowerCase();

            if (loginNameToLowerCase.indexOf(this.loginNameBusquedaToLowerCase) != -1) {
                usuariosByUsuario.push(user);

            }

        });


        usuariosByUsuario.forEach(user => {

            let userNameToLowerCase = user.name.toLowerCase();

            if (userNameToLowerCase.indexOf(this.userNameBusquedaToLowerCase) != -1) {
                usuariosByUsuarioUserName.push(user);
            }

        });

        usuariosByUsuarioUserName.forEach(user => {

            user.roles.forEach(roles => {
                let rolNameToLowerCase = roles.description.toLowerCase();
                if (rolNameToLowerCase.indexOf(this.rolNameBusquedaToLowerCase) != -1) {
                    usuariosByUsuarioUserNameRol.push(user);

                }

            });


        });

        this.grupoUsuarios = usuariosByUsuarioUserNameRol;

    }

    busquedaUserNameAndRol() {

        let usuariosByUserName: any[] = [];
        let usuariosByUserNameAndRol: any[] = [];
        this.usuariosEntidad.body.body.forEach(user => {

            let userNameToLowerCase = user.name.toLowerCase();

            if (userNameToLowerCase.indexOf(this.userNameBusquedaToLowerCase) != -1) {
                usuariosByUserName.push(user);

            }
        });
        usuariosByUserName.forEach(user => {
            user.roles.forEach(roles => {
                let rolNameToLowerCase = roles.description.toLowerCase();
                if (rolNameToLowerCase.indexOf(this.rolNameBusquedaToLowerCase) != -1) {
                    usuariosByUserNameAndRol = usuariosByUserName
                }

            });


        });
        this.grupoUsuarios = usuariosByUserNameAndRol;
    }

    busquedaUsuarioUserName() {

        let usuariosByUsuario: any[] = [];
        let usuariosByUsuarioAndUserName: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {

            let loginNameToLowerCase = user.userName.toLowerCase();

            if (loginNameToLowerCase.indexOf(this.loginNameBusquedaToLowerCase) != -1) {
                usuariosByUsuario.push(user);
            }

        });

        usuariosByUsuario.forEach(user => {

            let userNameToLowerCase = user.name.toLowerCase();

            if (userNameToLowerCase.indexOf(this.userNameBusquedaToLowerCase) != -1) {
                usuariosByUsuarioAndUserName = usuariosByUsuario;

            }

        });

        this.grupoUsuarios = usuariosByUsuarioAndUserName;
    }

    busquedaUsuarioAndRol() {

        let usuariosByUsuario: any[] = [];
        let usuariosByUsuarioAndRol: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {

            let loginNameToLowerCase = user.userName.toLowerCase();

            if (loginNameToLowerCase.indexOf(this.loginNameBusquedaToLowerCase) != -1) {
                usuariosByUsuario.push(user);
            }

        });

        usuariosByUsuario.forEach(user => {
            user.roles.forEach(roles => {
                let rolNameToLowerCase = roles.description.toLowerCase();
                if (rolNameToLowerCase.indexOf(this.rolNameBusquedaToLowerCase) != -1) {
                    usuariosByUsuarioAndRol = usuariosByUsuario
                }

            });


        });

        this.grupoUsuarios = usuariosByUsuarioAndRol;


    }

    busquedaUsuario() {

        let usuariosByUsuario: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {

            let loginNameToLowerCase = user.userName.toLowerCase();

            if (loginNameToLowerCase.indexOf(this.loginNameBusquedaToLowerCase) != -1) {
                usuariosByUsuario.push(user);

            }

        });
        this.grupoUsuarios = usuariosByUsuario;
    }

    busquedaRol() {
        let usuariosByRolName: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {
            user.roles.forEach(roles => {
                let rolNameToLowerCase = roles.description.toLowerCase();
                if (rolNameToLowerCase.indexOf(this.rolNameBusquedaToLowerCase) != -1) {
                    usuariosByRolName.push(user);
                }
            });
        });

        let usuariosSinRepetir: any[] = [];

        for (let usuario of usuariosByRolName) {

            if (usuariosSinRepetir.length == 0) {
                usuariosSinRepetir.push(usuario);
                continue;
            }

            let esRepetido = false;
            for (let usuarioSinRepetir of usuariosSinRepetir) {
                if (usuarioSinRepetir.userName == usuario.userName) {
                    esRepetido = true;
                    break;
                }
            }
            if (!esRepetido) {
                usuariosSinRepetir.push(usuario);
            }
        }

        this.grupoUsuarios = usuariosSinRepetir;
    }

    busquedabyUserName() {

        let usuariosByUserName: any[] = [];

        this.usuariosEntidad.body.body.forEach(user => {

            let userNameToLowerCase = user.name.toLowerCase();

            if (userNameToLowerCase.indexOf(this.userNameBusquedaToLowerCase) != -1) {
                usuariosByUserName.push(user);

            }

        });
        this.grupoUsuarios = usuariosByUserName;
    }

    nuevoUsuario() {
        this.usuarioPadre = new UserDTO()
        this.index = -1;
    }

    editarUsuario(user, index) {
        this.usuarioPadre = user;
        this.index = index;
    }

    mensajeEditado() {
        this.msgServicio = new MessageCustomeDTO();
        this.msgServicio.severity = 'success';
        this.msgServicio.summary = '¡Correcto!';
        this.msgServicio.detail = 'Datos actualizados Correctamente';
        this.msgServicio.life = timeComponent.timeFadeOutMessageComponent;
        this.msgServicio.closable = false;
    }

    guardarUsuario(newUser: UserDTO) {

        if (newUser != null) {
            this.entidadSeleccionada = false;
            this.mensajeEditado();
            this.fn.updateItemByEntity(this.grupoUsuarios, newUser, this.index);
        }
        this.ngOnInit();
        this.usuarioPadre = null;
        this.index = -1;
    }

    cerrarPopUp(display: boolean) {
        if (display == false) {
            this.usuarioPadre = null;
            this.index = -1;
        }
    }

    eliminarUsuario(usuario
                        :
                        UserDTO
    ) {
        this.confirmationService.confirm({
            message: '¿Realmente desea eliminar el registro?',
            accept: () => {
                const userCO = {user: usuario}
                this.usuarioService.eliminarUsuario(userCO).subscribe(data => {
                });
                this.grupoUsuarios = this.fn.deleteItemByEntity(this.grupoUsuarios, usuario);
            },
        });
    }

    valorLista(item
                   :
                   string, lista
                   :
                   any[]
    ) {
        const itemNvp = this.fn.filterItemByListNvp(lista, item);
        return !!itemNvp ? itemNvp.name : '';
    }


    cargarEntidades(query, entidad: any[]): any[] {
        this.listaEntidades = new Array<NameValuePair>();
        let codCoopGeneral = new NameValuePair();
        let filtered: any[] = [];
        codCoopGeneral.value = gestionCodEntidad.ENTIDAD.codigoEntidad;
        codCoopGeneral.name = gestionCodEntidad.ENTIDAD.nombreEntidad;
        this.listaEntidades.push(codCoopGeneral);

        for (var i in  entidad) {
            var nvpGroup = new NameValuePair();
            nvpGroup.name = entidad[i].nombreEntidad;
            nvpGroup.value = entidad[i].codigoEntidad;
            this.listaEntidades.push(nvpGroup);
        }

        this.listaEntidades.forEach(entidad => {
            if (!!entidad && query != "" && query != null) {
                if (entidad.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                    filtered.push(entidad);
                } else if (!!entidad && query == null) {
                    query = " ";
                    if (entidad.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                        filtered.push(entidad);
                    }
                }
            }
        })

        return filtered;


    }

    filterEntidadySelect(event) {
        this.query = event.query;
        if (this.query == undefined || this.query == "") {
            this.query = " ";
        }
        this.usuarioService.getEntidades().subscribe(entidad => {
            this.filtroEntidades = this.cargarEntidades(this.query, entidad.body);
        });
    }


    cambiarDecision() {

        if (this.codigoEntidad != this.entidad.value) {
            this.codigoEntidad = this.entidad.value;
            this.entidadSeleccionada = true;
            this.botonBuscar = false;
            this.grupoUsuarios = null;
        } else if (this.codigoEntidad == null) {
            this.botonBuscar = true;
        } else if (this.codigoEntidad == this.entidad.value) {

        } else {
            this.botonBuscar = true;
        }

    }

    nuevaBusquedaByEntidad() {
        this.entidadSeleccionada = true;
    }
}
