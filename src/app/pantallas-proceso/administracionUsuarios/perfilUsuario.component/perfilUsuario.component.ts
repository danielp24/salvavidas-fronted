import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {LoginService} from "src/app/services/login.service";
import {UserDTO} from "src/app/modelo/dto/userDTO";
import {UtilitiesFnService} from "src/app/infraestructura/utils/utilitiesFn";
import {NameValuePair} from "src/app/modelo/so/nombreValor";
import {UsuarioService} from "src/app/services/usuario.service";
import {apiUser, successResponse, timeComponent} from "src/environments/environment.variables";
import {headerDTO} from "../../../modelo/dto/common/domain/generic/user/headerDTO";
import {RolesDTO} from "../../../modelo/dto/rolesDTO";
import {UpdateDTO} from "../../../modelo/dto/updateDTO";
import {UpdateBody} from "../../../modelo/dto/updateBody";
import {Message, MessageService} from "primeng/api";
import {MessageCustomeDTO} from "../../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-perfilUsuario',
    styleUrls: ['./perfilUsuario.component.css'],
    templateUrl: './perfilUsuario.component.html'
})
export class PerfilUsuarioComponent implements OnInit {
    loader: boolean;
    formUser: FormGroup;
    display: boolean = true;
    editarVsb: boolean = false;
    currentUser: UserDTO;
    tipoIdentList: NameValuePair[];
    mensajeConfirmacion: Message;

    // @ts-ignore
    @Output() closePopupPerfil = new EventEmitter<boolean, boolean>();

    constructor(
        private _formBuilder: FormBuilder,
        private router: Router,
        private loginService: LoginService,
        private fn: UtilitiesFnService,
        private usuarioService: UsuarioService,
        private messageService: MessageService,
    ) {
        this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

        this.formUser = new FormGroup({
            identType: new FormControl('NIT', []),
            identNumber: new FormControl('', []),
            name: new FormControl('', [Validators.required]),
            lastName: new FormControl('', []),
            fullName: new FormControl('', []),
            email: new FormControl('', [Validators.required, Validators.pattern('[A-Za-z-0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$'),]),
            role: new FormControl('', []),
            newRole: new FormControl('', []),
            phoneNumber: new FormControl('', [
                Validators.required,
            ]),
        });


        /*   this.formUser = this._formBuilder.group({
               identType: '',
               identNumber: '',
               name: '',
               lastName: '',
               fullName: '',
               email: '',
               role: '',
               newRole: '',
               phoneNumber: ''
           });*/

        this.tipoIdentList = [
            {name: 'Cédula Ciudadanía', value: '1'},
            {name: 'Tarjeta Identidad', value: '2'},
            {name: 'Tarjeta Pasaporte', value: '3'},
            {name: 'Registro Civil', value: '4'},
            {name: 'Cédula de Extranjería', value: '5'}
        ];
    }

    ngOnInit(): void {

        this.formUser.disable();

        this.onChargeEntity(this.currentUser);

        //this.cargarGrupos();

        this.onChargeForm();
    }

    onChargeEntity(user: UserDTO) {

    }

    onChargeForm() {
        this.formUser.valueChanges
            .subscribe(val => {
                    this.fn.formToDto(val, this.currentUser);
                }, error => console.log(error)
            );
    }

    aceptarPerfil() {
        let cambioData: boolean = false;
        if (this.formUser.invalid) {
            return;
        }
        if (!!this.editarVsb) {
            this.loader = true;
            //   const userCO = { user: this.currentUser }
            const userCOE = new UpdateDTO();
            userCOE.header = new headerDTO;
            userCOE.body = new UpdateBody();
            let rolesUsers = new RolesDTO;
            const idUserRoles: string[] = [];
            userCOE.header.apiToken = apiUser.tokenApplication;
            userCOE.body.id = this.currentUser.body.id;
            userCOE.body.username = this.currentUser.body.username;
            userCOE.body.email = this.currentUser.body.email;
            userCOE.body.name = this.currentUser.body.name;
            userCOE.body.phoneNumber = this.currentUser.body.phoneNumber;
            userCOE.body.ldapGuid = null;
            userCOE.body.organization = this.currentUser.body.organization;
            //   userCOE.body.roles = this.currentUser.body.roles;
            for (var rol in this.currentUser.body.roles) {
                rolesUsers = JSON.parse(JSON.stringify(this.currentUser.body.roles[rol]));
                idUserRoles.push(rolesUsers.id)
            }
            userCOE.body.roles = idUserRoles;
            this.usuarioService.editarUsuario(userCOE)
                .subscribe(data => {
                        if (!!data && !!data.body) {
                        }
                        if (data.body.status == successResponse.statusCode) {
                            this.loader = false;
                            this.mensajeEditado();
                            sessionStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                            setTimeout(() => {
                                this.display = false, this.closePopupPerfil.emit(false);
                            }, 2000)

                        }
                    }, error => console.log(error)
                );
        }
    }

    mensajeEditado() {
        this.mensajeConfirmacion = new MessageCustomeDTO();
        this.mensajeConfirmacion.severity = 'success';
        this.mensajeConfirmacion.summary = '¡Correcto!';
        this.mensajeConfirmacion.detail = 'Datos actualizados';
        this.mensajeConfirmacion.life = timeComponent.timeFadeOutMessageComponent;
        this.mensajeConfirmacion.closable = true;

    }

    editarPerfil() {
        this.formUser.enable();
        this.editarVsb = true;
    }

    logout() {
        this.loginService.logout();
        this.router.navigate(['/login']);
    }

    load() {
        if (typeof (Storage) != "undefined") {
        }
    }

    cerrarActualizar() {
        this.display = false, this.closePopupPerfil.emit(false);
    }
}
