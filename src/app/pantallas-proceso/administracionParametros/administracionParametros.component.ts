import {Component, OnInit} from '@angular/core';
import {NameValuePair} from "../../modelo/so/nombreValor";
import {
    apiUser,
    gestionCodEntidad,
    recuperarContraseña,
    timeComponent
} from "../../../environments/environment.variables";
import {ParametrosService} from "../../services/parametros.service";
import {listaParametros} from "../../../environments/environment.variables";
import {Message} from "primeng/api";
import {UserDTO} from "../../modelo/dto/userDTO";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {RolesDTO} from "../../modelo/dto/rolesDTO";
import {ParametrosDominioDTO} from "../../modelo/dto/parametrosDominioDTO";
import {BodyParametroDTO} from "../../modelo/dto/BodyParametroDTO";
import {DeshabilitarParametroDTO} from "../../modelo/dto/DeshabilitarParametroDTO";
import {BodyParametroInsertarDTO} from "../../modelo/dto/BodyParametroInsertarDTO";
import {UtilitiesFnService} from "../../infraestructura/utils/utilitiesFn";
import {GetListParametrosGeneralDTO} from "../../modelo/dto/getListParametrosGeneralDTO";
import {MessagesAlertTimeFadeOut} from "../../infraestructura/utils/messagesAlertTimeFadeOut/messages-alert-time-fade-out.component";
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-administracionParametros',
    templateUrl: './administracionParametros.component.html'
})
export class AdministracionParametrosComponent implements OnInit {

    loader: boolean;
    listaParametros: NameValuePair[];
    parametro: NameValuePair;
    filtroParametros: any[];
    codigoParametro: string;
    botonBuscar: boolean = true;
    msgServicio: MessageCustomeDTO;
    grupoParametros: ParametrosDominioDTO[];
    grupoParametrosInactivos: ParametrosDominioDTO[];
    usuarioPadre: BodyParametroDTO;
    formUser: FormGroup;
    index: number = -1;
    currentUser: UserDTO;
    roles: RolesDTO;
    i: number;
    entidadSeleccionada: boolean = true;
    codigoEntidad: string;
    codigoIdPadre: any;
    userName: string;
    datosDominio: any[] = [];
    creable: boolean = true;
    editable: boolean = true;
    disabled: boolean = false;
    idParametros: any[] = [];
    activarHistorico: boolean = true;
    agregarParametro: boolean = false;
    formNuevoParametro: FormGroup;
    dominioActual: string;
    parametrosActivos: any[] = [];
    popUpHistorico: boolean = false;
    botonCrear = "";
    query: string;
    idPadres: NameValuePair[];
    selectIdPadre: NameValuePair;
    campoIdPadre: boolean;
    indexDeshabilitar: number;
    dominioConsultar = new GetListParametrosGeneralDTO();
    mostrarMensaje: boolean = true;


    constructor(private servicioParametros: ParametrosService,
                private fn: UtilitiesFnService,
    ) {
        this.initForm();
    }

    ngOnInit() {
        this.loader = true;
        setTimeout(() => {
            this.loader = false;
        }, 1000);
        console.log("parametros" + this.grupoParametros);
    }

    private initForm() {

        this.formNuevoParametro = new FormGroup({
            nombre: new FormControl('', [
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(100)
            ]),
            descripcion: new FormControl('', [
                Validators.required,
                Validators.maxLength(255),
                Validators.minLength(1)
            ]),
            valor: new FormControl('', [
                Validators.maxLength(255),
                Validators.minLength(1)
            ]),
            IdPadre: new FormControl('', [
                Validators.required,

            ])


        });
    }


    cargarParametros(query, parametros: any[]): any[] {
        this.listaParametros = new Array<NameValuePair>();
        let filtered: any[] = [];

        for (var i in parametros) {
            var nvpGroup = new NameValuePair();
            nvpGroup.name = parametros[i].descripcion;
            nvpGroup.value = parametros[i].idDominio;
            this.listaParametros.push(nvpGroup);
        }

        this.listaParametros.forEach(parametro => {
            if (!!parametro && query != "" && query != null) {
                if (parametro.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                    filtered.push(parametro);
                } else if (!!parametro && query == null) {
                    query = " ";
                    if (parametro.name.toLowerCase().indexOf(query.toLowerCase()) != -1) {
                        filtered.push(parametro);
                    }
                }

            }
        })

        return filtered;


    }

    filterParametroSelect(event) {

        let codParametro: string;
        codParametro = listaParametros.parametrosGeneral.codigo;
        this.query = event.query;
        if (this.query == undefined || this.query == "") {
            this.query = " ";
        }
        this.servicioParametros.getParametros(codParametro).subscribe(parametro => {
            this.datosDominio = parametro.body;
            this.filtroParametros = this.cargarParametros(this.query, parametro.body);
        });

    }


    cambiarDecision() {
        let editable = "";
        let creable = "";
        let disable = "";
        if (this.codigoParametro != this.parametro.value) {
            this.codigoParametro = this.parametro.value;
            this.agregarParametro = false;
            this.activarHistorico = true;
            this.creable = true;
            this.codigoParametro = this.parametro.value;
            this.entidadSeleccionada = true;
            this.botonBuscar = false;
            this.grupoParametros = null;
            this.datosDominio.forEach(dominio => {
                if (this.parametro.value == dominio.idDominio) {
                    editable = dominio.editable;
                    creable = dominio.creable;
                    disable = dominio.disable;
                }

            })
            if (editable == listaParametros.dominioActivo.codigo) {
                this.editable = false;
            } else {
                this.editable = true;
            }
            if (disable == listaParametros.dominioActivo.codigo) {
                this.disabled = false;
            } else {
                this.disabled = true;
            }
            if (creable == listaParametros.dominioActivo.codigo) {
                this.botonCrear = creable;
            }

        } else if (this.codigoEntidad == null) {
            this.botonBuscar = true;
        } else if (this.codigoEntidad == this.parametro.value) {

        } else {
            this.botonBuscar = true;
        }

    }

    selectIPadre() {
        this.codigoIdPadre = this.selectIdPadre.value;


    }

    getIdPadreParametro(data: any) {
        if (!!data) {
            this.idPadres = new Array<NameValuePair>();
            let listIdPadres: NameValuePair[] = [];
            for (let i = 0; i < data.body.length; i++) {
                let idPadreDescripcion = new NameValuePair();
                idPadreDescripcion.name = data.body[i].descripcion;
                idPadreDescripcion.value = data.body[i].idParametro;
                idPadreDescripcion.descripcion = data.body[i].descripcion;

                listIdPadres.push(idPadreDescripcion);

            }
            let idPadreSinRepetir: NameValuePair[] = [];
            for (let idPadre of listIdPadres) {
                if (idPadreSinRepetir.length == 0) {
                    idPadreSinRepetir.push(idPadre);
                    continue;
                }
                let esRepetido = false;
                for (let usuarioNoRepetido of   idPadreSinRepetir) {
                    if (usuarioNoRepetido.value == idPadre.value) {
                        esRepetido = true;
                        break;
                    }
                }
                if (!esRepetido) {
                    //idPadreSinRepetir.push(idPadre);
                    idPadreSinRepetir.push(idPadre);
                }

            }
            let idPadreNulo = new NameValuePair();
            idPadreNulo.value = listaParametros.idPadreNulo.value;
            idPadreNulo.name = listaParametros.idPadreNulo.name
            idPadreSinRepetir.push(idPadreNulo);
            this.idPadres = idPadreSinRepetir;


        } else {
            this.campoIdPadre = true;
        }
    }

    buscarParametros() {
        this.loader = true;
        this.idParametros = [];
        this.parametrosActivos = [];
        let codDominio = this.codigoParametro;

        for (let i of this.datosDominio) {
            if (i.idDominio == this.parametro.value) {

                this.dominioConsultar.idDominio = parseInt(codDominio);
                this.dominioConsultar.disabled = i.disable;
            }
            if (i.idDominio == this.parametro.value && i.disable == listaParametros.dominioNoActivo.codigo) {
                this.activarHistorico = true;

            } else {
                this.activarHistorico = false;
            }

            if (i.idDominio == this.parametro.value && i.creable == listaParametros.dominioNoActivo.codigo) {
                this.creable = true;

            }
            if (i.idDominio == this.parametro.value && i.creable != listaParametros.dominioNoActivo.codigo) {
                this.creable = false;

            }


        }

        this.servicioParametros.getParametrosIdDOminio(this.dominioConsultar).subscribe(data => {
            this.getIdPadreParametro(data);
            if (!data) {
                this.msgServicio = new MessageCustomeDTO();
                this.msgServicio.severity = 'error';
                this.msgServicio.summary = '¡Aviso!';
                this.msgServicio.detail = apiUser.msgServiceListOrg;
                this.msgServicio.life = timeComponent.timeFadeOutMessageComponent;;
                this.msgServicio.closable = false;

                this.entidadSeleccionada = true;
                this.loader = false;
                this.dominioActual = (String)(this.dominioConsultar.idDominio);

            } else {
                this.mostrarMensaje = false;
                this.botonBuscar = true;
                this.loader = false;
                this.entidadSeleccionada = false;

                for (let i = 0; i < data.body.length; i++) {
                    if (data.body[i].idPadre == null) {
                        data.body[i].idPadre = "No aplica";
                    } else {
                        data.body[i].idPadre = data.body[i].idPadre;
                    }
                    if (data.body[i].idDominio == null) {
                        data.body[i].idDominio = "No aplica";
                    } else {
                        data.body[i].idDominio = data.body[i].idDominio;
                        this.dominioActual = data.body[i].idDominio
                    }
                    if (data.body[i].valor == null) {
                        data.body[i].valor = "No aplica";
                    } else {
                        data.body[i].valor = data.body[i].valor;
                    }
                    if (data.body[i].descripcion == null) {
                        data.body[i].descripcion = "No aplica";

                    } else {
                        data.body[i].descripcion = data.body[i].descripcion;
                    }
                    if (data.body[i].nombre == null) {
                        data.body[i].nombre = "No aplica";

                    } else {
                        data.body[i].nombre = data.body[i].nombre;
                    }

                    data.body[i].disabled = data.body[i].disabled;
                    this.idParametros.push(data.body[i]);


                }

                this.idParametros.forEach(parametro => {

                    if (parametro.disabled == listaParametros.dominioNoActivo.codigo) {
                        this.parametrosActivos.push(parametro);
                    }

                });

                this.grupoParametros = this.parametrosActivos;

            }
        });
    }

    editarParametro(parametro, index) {
        this.usuarioPadre = null;
        this.usuarioPadre = parametro;
        this.index = index;
    }

    historicoParametros() {
        this.popUpHistorico = true;
        let parametrosNoActivos: any[] = [];
        this.idParametros.forEach(parametro => {
            if (parametro.disabled == listaParametros.dominioActivo.codigo) {
                parametrosNoActivos.push(parametro);
            }
        })

        this.grupoParametrosInactivos = parametrosNoActivos;

    }

    deshabilitarParametro(parametro, index) {
        const codigo = new DeshabilitarParametroDTO();
        codigo.idParametro = parametro.idParametro;

        this.servicioParametros.deshabilitarParametro(codigo).subscribe(data => {
            if (!!data) {

                console.log(data);
                this.buscarParametros();
                this.mensajeEditado();
                if (this.popUpHistorico == true) {
                    this.indexDeshabilitar = index;
                    if (index > -1) {
                        this.grupoParametrosInactivos.splice(this.indexDeshabilitar, 1)
                    }
                    this.grupoParametrosInactivos = [...this.grupoParametrosInactivos];
                }

                this.loader = false;

                // hola soy camilo y soy estilo rcn :3

            }
        })


    }

    cerrar() {
        if (this.popUpHistorico == true) {
            this.popUpHistorico = false;
        }
        if (this.agregarParametro == true) {
            this.agregarParametro = false;
        }
    }

    activarPopUpAgregarParametro() {
        this.agregarParametro = true;
        this.initForm();
    }

    nuevoParametro() {
        const nuevoParametro = new BodyParametroInsertarDTO();

        nuevoParametro.idDominio = this.dominioActual;
        if (this.codigoIdPadre == null || this.codigoIdPadre == "") {
            this.codigoIdPadre = 0;
        }
        nuevoParametro.idPadre = parseInt(this.codigoIdPadre);
        nuevoParametro.nombreP = this.formNuevoParametro.get('nombre').value;
        nuevoParametro.valorP = this.formNuevoParametro.get('valor').value;
        nuevoParametro.descripcionP = this.formNuevoParametro.get('descripcion').value;

        this.servicioParametros.agregarParametro(nuevoParametro).subscribe(data => {

            this.cerrar();
            this.buscarParametros();

        })
      this.mensajeEditado();

    }

    guardarParametro(newUser: BodyParametroDTO) {

        if (newUser != null) {
            const userCO = newUser;
            if (this.index == -1) {
                this.servicioParametros.actualizarParametro(userCO).subscribe(data => {
                });
            } else {
                this.servicioParametros.actualizarParametro(userCO).subscribe(data => {
                    this.entidadSeleccionada = false;
                    this.mensajeEditado();
                    this.buscarParametros();
                });


            }

            this.fn.updateItemByEntity(this.grupoParametros, newUser, this.index);

        }
        this.usuarioPadre = null;
        this.index = -1;
    }

    mensajeEditado() {
        this.msgServicio = new MessageCustomeDTO();
        this.msgServicio.severity = 'success';
        this.msgServicio.summary = '¡Correcto!';
        this.msgServicio.detail = 'Datos actualizados Correctamente';
        this.msgServicio.life = timeComponent.timeFadeOutMessageComponent;
        this.msgServicio.closable = false;
    }

    ocultarColumnaAcciones() {
        if (this.editable && this.disabled == true) {
            return false;
        } else {
            return true;
        }
    }

}
