import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BodyParametroDTO} from "../../../modelo/dto/BodyParametroDTO";
import {ParametrosService} from "../../../services/parametros.service";

@Component({
    selector: 'app-detalle-administracion-parametros',
    templateUrl: './detalle-administracion-parametros.component.html',
    styleUrls: ['./detalle-administracion-parametros.component.css']
})
export class DetalleAdministracionParametrosComponent implements OnInit {
    @Input() usuarioHijo: BodyParametroDTO;
    @Output() guardarParametro = new EventEmitter<BodyParametroDTO>();
    display: boolean = false;
    idNoNull: boolean = false;

    constructor(private servicioParametros: ParametrosService) {


    }

    ngOnInit() {

        this.editarParametro();
    }

    editarParametro() {
        this.display = true;
        if (this.usuarioHijo.idPadre != "No aplica") {
            this.idNoNull = true;

        }
        console.log(this.usuarioHijo.valor);

    }

    cerrarPopUp() {

        this.usuarioHijo = null;
        this.guardarParametro.emit(this.usuarioHijo);

    }

    actualizarParametro() {

        const parametro = new BodyParametroDTO();
        parametro.valor = this.usuarioHijo.valor;
        parametro.descripcion = this.usuarioHijo.descripcion;
        if (this.usuarioHijo.idPadre == "No aplica") {
            parametro.idPadre = 0;
        } else {
            parametro.idPadre = this.usuarioHijo.idPadre;
        }
        parametro.idParametro = parseInt(this.usuarioHijo.idParametro);
        parametro.nombre = this.usuarioHijo.nombre;

        this.servicioParametros.actualizarParametro(parametro).subscribe(data => {
            console.log(data);
            this.display = false;
           this.guardarParametro.emit(parametro);

        })
    }
}
