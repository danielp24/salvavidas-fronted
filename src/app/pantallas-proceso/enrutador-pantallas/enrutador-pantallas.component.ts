import {environment} from "../../../environments/environment";
import {DomSanitizer} from "@angular/platform-browser";
import {ActivatedRoute} from "@angular/router";
import {Component} from "@angular/core";
import {BussinesParameterService} from "src/app/services/bussinesParameters.service";
import {Message} from "primeng/api";

@Component({
    selector: 'app-enrutador-pantallas',
    templateUrl: './enrutador-pantallas.component.html',
    styleUrls: ['./enrutador-pantallas.component.css']
})

export class EnrutadorPantallasComponent {

    private bussinesParamService: BussinesParameterService;
    msgServicio: Message[] = [];
    loader: boolean;

    constructor(public sanitizer: DomSanitizer,
                public  route: ActivatedRoute) {
        this.bussinesParamService = new BussinesParameterService();
        this.loader = true;
        setTimeout(() => {
            this.loader = false;
        }, 1000);
    }

    iframePath() {

        this.bussinesParamService.homologarData();
        let urlIframe = this.route.snapshot.params["redirect"] + "?param=" + btoa(JSON.stringify(this.bussinesParamService.getBussinessParams()));
        let url = environment.iframesPath + urlIframe;
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);

    }

}

