import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserDTO} from "../../modelo/dto/userDTO";
import {headerDTO} from "../../modelo/dto/common/domain/generic/user/headerDTO";
import {BodyDTO} from "../../modelo/dto/common/domain/generic/user/bodyDTO";
import {apiUser, timeComponent} from "../../../environments/environment.variables";
import {Message} from "primeng/api";
import {LoginService} from "../../services/login.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ROUTES_PATH} from "../../app.routes.names";
import {ChangedPassService} from "../../services/changed-pass.service";
import {AlertService} from '../../infraestructura/utils/alert.service';
import {invalid} from "moment";
import {MessageCustomeDTO} from "../../modelo/so/messageCustomeDTO";

@Component({
    selector: 'app-recover-password',
    templateUrl: './recover-password.component.html',
    styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {
    mensajePoliticas: string;
    ocultarContenido: boolean = false;
    token: string;
    decodificarToken: string;
    recuperar = true;
    formRecover: FormGroup;
    error: Message[] = [];
    mensaje: string = "";
    usuario: string = "";
    password: boolean;
    loader: boolean;
    infoPopUp: boolean;
    contrasenasIguales: boolean;
    error1: boolean;
    mensajeCambioContrasena: MessageCustomeDTO;


    constructor(
        private changePassService: ChangedPassService,
        private router: Router,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
        private route: ActivatedRoute,
        private alertService: AlertService
    ) {
        this.route.queryParams.subscribe(params => {
            this.token = params['token'];
        });
        this.decodificarToken = atob(this.token);
        let usuario = this.decodificarToken.split(",");
        this.usuario = usuario[0];


    }

    ngOnInit() {
        this.loader = true;
        this.validarToken();

        this.formRecover = this.formBuilder.group({
            username: ['', Validators.nullValidator],
            password: ['', Validators.required],
            confirmarPassword: ['', Validators.required],
        }, {validator: this.checkIfMatchingPasswords('password', 'confirmarPassword')});
        this.politicasNewPassword();
    }


    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[passwordKey],
                passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value && this.formRecover.get('confirmarPassword').value != "") {
                this.contrasenasIguales = true;
                return passwordConfirmationInput.setErrors({notEquivalent: true})

            } else {
                this.contrasenasIguales = false;
                return passwordConfirmationInput.setErrors(null);

            }
        }
    }


    submit() {
        this.loader = true;
        if (this.formRecover.invalid || this.password == true) {
            this.loader = false;
            return;
        }

        let user = new UserDTO();
        let header = new headerDTO();
        let body = new BodyDTO();
        body.username = this.usuario;
        body.password = this.formRecover.get('password').value;
        header.apiToken = apiUser.tokenApplication;

        user.header = header;
        user.body = body;
        this.loginService.recoverPassword(user).subscribe(data => {
            console.log(data);
            this.mensaje = data.message;
            if (data.status == 'OK') {
                this.infoPopUp = true;

                this.mensajeCambioContrasena = new MessageCustomeDTO();
                this.mensajeCambioContrasena.severity = 'success';
                this.mensajeCambioContrasena.summary = '¡Correcto!';
                this.mensajeCambioContrasena.detail = 'Se ha realizado la recuperacion de la contraseña, por favor ingrese nuevamente.';
                this.mensajeCambioContrasena.life = timeComponent.timeFadeOutMessageComponent;
                this.mensajeCambioContrasena.closable = false;
            }
            this.loader = false;
        }, error => {
            console.log("Prueba error", error)
            this.loader = false;
        });
    }

    validarToken() {

        this.loginService.validarToken(this.token).subscribe(data => {
            if (data === "") {
                this.loader = false;
                this.ocultarContenido = false;
                setTimeout(() => {
                        this.volverLogin();
                    },
                    5000);
            } else {
                if (data.body.pexiste == 1) {
                    this.ocultarContenido = true;
                    this.loader = false;
                }
            }

        }, j => {
            this.loader = false;
        })
        ;
    }

    volverLogin() {
        this.router.navigate(['/' + ROUTES_PATH.login]);
    }

    politicasNewPassword() {
        let header = new headerDTO();
        header.apiToken = apiUser.tokenApplication;
        this.changePassService.policesNewPassword(header).subscribe(data => {
            this.loader = false;
            this.mensajePoliticas = " 1.La contraseña requiere mayúscula: \n " + data.body.body.passwordRequireUppercase + ".\n"
                + " 2.Cantidad minima de caracteres: \n" + data.body.body.passwordRequiredLength + " caracteres.\n"
                + " 3.Expiración contraseña: \n" + data.body.body.passwordExpiredDays + " días.\n"
                + " 4.Requiere numero, letra o digito: \n" + data.body.body.passwordRequireNonLetterOrDigit + ".\n"
                + " 5.Requiere dígito: \n" + data.body.body.passwordRequireDigit + ".\n"
                + " 6.Requiere Minusculas: \n" + data.body.body.passwordRequireLowercase + ".\n"
                + " 7.No se permiten Caracteres tildados y letra ñ/Ñ"


        }, error1 => {
            this.loader = false;
            this.mensaje = "HA OCURRIDO UN ERROR";
        })
    }

    validarCaracteres() {
        let noValido: boolean = false;
        this.mensaje = "";
        let cadena = this.formRecover.get('password').value;
        for (var i = 0; i < cadena.length; i++) {
            var caracter = cadena.charAt(i);

            if (caracter == "ñ" || caracter == "Ñ" ||
                caracter == "á" || caracter == "Á" ||
                caracter == "á" || caracter == "Á" ||
                caracter == "é" || caracter == "É" ||
                caracter == "í" || caracter == "Í" ||
                caracter == "ó" || caracter == "Ó" ||
                caracter == "ú" || caracter == "Ú") {
                this.mensaje = "Caracter " + cadena.charAt(i) + " no permitido";
                this.error1 = true;
                return this.password = true;

            } else {
                this.error1 = false;
                return this.password = false;
            }
        }
    }

    cerrarPoliticas() {
        this.mensaje = "";
        this.infoPopUp = false;
        this.volverLogin();
    }
}
