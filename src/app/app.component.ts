import { Component } from '@angular/core';
import { UserDTO } from './modelo/dto/userDTO';
import { LoginService } from './services/login.service';
import { Observable } from 'rxjs';
import { LoadingService } from './infraestructura/utils/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  currentUser: UserDTO;
  isLoading: boolean;
  loading$: Observable<boolean>;

  constructor(
    private loginService: LoginService,
    private loading: LoadingService
  ) {
    this.loginService.currentUser.subscribe(x => this.currentUser = x);

    this.loading$ = this.loading.getLoaderAsObservable();

    this.loading$.subscribe(value => {
      this.isLoading = value;
    });
  }
}
