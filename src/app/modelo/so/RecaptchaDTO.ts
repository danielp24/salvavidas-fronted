export class RecaptchaDTO {
    response: string;
    secretKey: string;
}
