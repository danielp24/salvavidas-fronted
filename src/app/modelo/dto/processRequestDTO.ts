import { ParametrosDTO } from "./parametrosDTO";
import { UserBPMDTO } from "./UserBPMDTO";

export class ProcessRequestDTO {
	containerId?: string;
	processesId?: string;
	processInstance?: string;
	taskId?: string;
	taskStatus?: string;
	groups?: string[];
	ownerUser?: UserBPMDTO;
	assignment?: UserBPMDTO;
	parametros?: ParametrosDTO;
	signal?: string;
	taskRoute ?: string;
}