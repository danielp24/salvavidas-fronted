export class PrivilegesDTO{
    name : string;
    id : string;
    enabled : boolean;
    code: string;
}
