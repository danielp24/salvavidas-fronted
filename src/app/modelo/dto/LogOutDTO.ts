import {headerDTO} from "./common/domain/generic/user/headerDTO";
import { BodyNoPassDTO} from "./common/domain/generic/user/bodyNoPassDTO";

export class LogOutDTO {

    header: headerDTO ;
    body: BodyNoPassDTO ;


}
