import {TokenDTO} from "./TokenDTO";

export class ReestablecerContraseñaDTO {
    email: string;
    tokenAutenticatedBusiness: TokenDTO;
}
