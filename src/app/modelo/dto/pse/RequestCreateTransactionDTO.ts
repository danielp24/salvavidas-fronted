

export class RequestCreateTransactionDTO {
    entityCode: string;
    srvCode: string;
    transValue: string;
    referenceArray: string[];
    urlRedirect: string;
}
