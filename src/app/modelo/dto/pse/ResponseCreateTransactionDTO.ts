import {BodyDTO} from "./BodyDTO";


export class ResponseCreateTransactionDTO {
    body: BodyDTO;
    returnCode: string;
    ticketId: string;
    ecollectUrl: string;
}
