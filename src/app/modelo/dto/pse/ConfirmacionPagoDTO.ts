

export interface ConfirmacionPagoDTO {
    entidad: string;
    NIT: string;
    fechaPago: Date;
    estadoPago: string;
    numTransaccion: number;
    banco: string;
    valorPagado: number;
    descripcion: string;
}
