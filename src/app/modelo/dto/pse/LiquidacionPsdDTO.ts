export interface LiquidacionPsdDTO {

    vr_total_a_pagar: number;
    vr_base_psd: number;
    vr_depositos: number;
    posicion_neta_val: number;
    saldo_a_favor_antes: number;
    saldo_a_cargo: number;
    interes_Mora: number;
    dias_Mora: number;
    tasa_Mora: number;
}
