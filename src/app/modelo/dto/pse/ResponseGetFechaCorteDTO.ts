export class ResponseGetFechaCorteDTO {
    fechaCorte: Date;
    fechaLimitePago: string;
    tasaMora: string;

}
