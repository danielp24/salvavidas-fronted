export class RequestPagosDTO {
    nit?: string;
    fechaCorte?: string;
    fechaPago: string;
    codFormaPago?: number;
    departamento?: number;
    municipio?: number;
    vrPagado?: number;
    observacion?: string;
    idTicket?: string;
    codigoBancoDestino?: number;
    estadoPago: number;
}
