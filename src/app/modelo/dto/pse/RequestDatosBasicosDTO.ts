export class RequestDatosBasicosDTO {
    numeroIdentificacion: number;
    codVerificacionNit: number;
    departamento: string;
    municipio: number;
    nombreCooperativa: string;
    direccionCooperativa: string;
    telefonoCooperativa: string;
    correoCooperativa: string;
    codeDepartamento: number;
    codeMunicipio: number;

}
