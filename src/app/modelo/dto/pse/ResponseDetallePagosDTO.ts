export interface ResponseDetallePagosDTO {
    codigoFormaPago?: number;
    nombreFormaPago?: string;
    nombreBanco?: string;
    nroChequeTransac?: string;
    codigoDepto?: string;
    nombreDepto?: string;
    codigoMunicipio?: string;
    nombreMunicipio?: string;
    fechaPago?: string;
    valorPagado?: string;
    observaciones?: string;
    aplicado?: string;
    codigoEstadoPago?: number;
    idTicket?: string;
    nombreEstadoPago?:string;

}
