import {BasicPagoDTO} from "../common/domain/generic/bussines/BasicPagoDTO";
import {OwnerUserPagoDTO} from "../common/domain/generic/bussines/OwnerUserPagoDTO";
import {TokenDTO} from "../TokenDTO";


export class VerificacionPagoDTO {
    ownerUser: OwnerUserPagoDTO;
    basic: BasicPagoDTO;
    origenPago: string;
    tokenAutenticatedBusiness: TokenDTO;

}
