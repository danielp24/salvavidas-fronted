export class RequestDetallePagoDTO {
    nit?: string;
    fechaCorte?: Date;
    estado?: string;
    organization?: string;
}
