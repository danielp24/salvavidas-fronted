export class BodyParametroDTO {

    idParametro?: any;
    idDominio?: any;
    idPadre?: any;
    nombre?: string;
    valor?: string;
    descripcion?: string;
    disabled?: any;
}
