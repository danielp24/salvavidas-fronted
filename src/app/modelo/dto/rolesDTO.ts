import {PrivilegesDTO} from "./privilegesDTO";

export class RolesDTO{

    name : string;
    id : string;
    description : string;
    enabled : boolean;
    codeAuthApplication : string;
    nameAuthApplication : string;
    privileges : PrivilegesDTO[];


}
