export class NotificacionDTO {
    numeroDocIdent: string;
    tipoDocIdent: string;
    canal: string;
    mensajeCorto?: string;
    mensajeLargo?: string;
    asunto?: string;
    adjunto?: any;
}