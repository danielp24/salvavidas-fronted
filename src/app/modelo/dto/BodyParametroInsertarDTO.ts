export class BodyParametroInsertarDTO {

    idParametro?: any;
    idDominio?: any;
    idPadre?: any;
    nombreP?: string;
    valorP?: string;
    descripcionP?: string;
    disabled?: any;
}
