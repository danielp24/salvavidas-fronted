export class ResponseGetBasesLiquidacion {
    prima : number;
    tasaPrima?: number;
    primaPosNeta?: number;
    tasaPrimaPosNeta?: number;
    sobrePrima?: number;
    tasaSobrePrima?: number;
    interesMora?: number;
    tasaInteresMora?: number;
    maxDiasInteresMora?: string;
}
