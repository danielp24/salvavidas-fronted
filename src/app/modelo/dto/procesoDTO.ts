import { InstanceDTO } from "./instanceDTO";
import { ParametrosDTO } from "./parametrosDTO";
import { TareaDTO } from "./tareaDTO";

export class ProcesoDTO {
	processId: string;
    processName: string;
    processVersion: string;
	startDate: BigInteger;
    initiator: string;
    packages: string;
	instance: InstanceDTO;
	parametros: ParametrosDTO;
	containerId: string;
	taskList: TareaDTO[];
}