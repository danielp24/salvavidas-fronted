export class ResponseGetDetalleLiquidacion {
    prima?: number;
    primaPosNeta?: number;
    sobrePrima?: number;
    saldoPendiente?: number;
    interesMora?: number;
    saldoFavor?: number;
    saldoFavorAplicadoPerAnt?: number;
    totalRetransmision?: number;
    valorTotalSugerido?: number;
    saldosPendientesAnteriores?: number;

}
