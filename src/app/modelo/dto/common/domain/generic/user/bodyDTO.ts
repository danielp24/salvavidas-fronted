import {RolesDTO} from "../../../../rolesDTO";

export class BodyDTO {

    public id?: string;
    public name?: string;
    public username?: string;
    public password?: string;
    public email?: string;
    public phoneNumber?: string;
    public userName?: string;
    public roles?: RolesDTO[];
    public enabled?: boolean;
    public ldapGuid?: string;
    public token?: string;
    public organization?: string;
    public lockoutEndDateUt?: Date;
    public lockoutEnabled?: boolean;

}
