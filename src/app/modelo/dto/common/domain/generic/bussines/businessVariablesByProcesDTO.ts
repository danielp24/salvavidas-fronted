import {ProcessCurrentDTO} from "./ProcessCurrentDTO";
import {TaskDTO} from "./taskDTO";
import {UserDTO} from "../../../../userDTO";

export class BusinessVariablesByProcesDTO{
    proccessCurrent: ProcessCurrentDTO;
    task: TaskDTO;
    currentUser?: UserDTO;
}
