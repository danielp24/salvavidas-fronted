export class TaskDTO {
    taskId: number;
    taskName: string;
    taskSubject: string;
    taskDescription: string;
    taskStatus: string;
    taskActualOwner: string;
    taskCreatedBy: string;
    taskCreatedOn: string;
    taskActivationTime: string;
    taskExpirationTime: string;
    taskProcDefId: string;
    containerId: string;
    instanceId: string;
}
