import {UserDTO} from "src/app/modelo/dto/userDTO";
import {BasicPagoDTO} from "./BasicPagoDTO";
import {TokenDTO} from "../../../../TokenDTO";

export class BusinessParametersDTO {
    currentUser?: UserDTO;
    basic?: BasicPagoDTO;


}
