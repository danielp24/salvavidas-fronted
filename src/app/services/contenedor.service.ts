import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { ProcessRequestDTO } from "../modelo/dto/processRequestDTO";
import { ProcessResponseDTO } from "../modelo/dto/processResponseDTO";

@Injectable()
export class ContenedorService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarContenedores (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.contenedor_endPoint + '/containers';
        return this._api.post(endpoint, request);
    }
}