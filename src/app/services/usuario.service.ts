import {Injectable} from "@angular/core";
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs";
import {UserDTO} from "../modelo/dto/userDTO";
import {apiUser} from "../../environments/environment.variables";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class UsuarioService {
    authorization: string;

    constructor(private _api: ApiManagerService) {

        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }


    consultarGrupos(user: UserDTO): Observable<any> {
        const endpoint = environment.listaRoles;
        user.header.apiToken = apiUser.tokenApplication;
        const showDisabled = true;
        const params = `showDisabled=${showDisabled}&apiToken=${user.header.apiToken}`

        return this._api.get(endpoint, params);
    }

    consultarUsuarios(user: UserDTO, codEntidad: string): Observable<any> {

        user.header.apiToken = apiUser.tokenApplication;

        const params = `?apiToken=${user.header.apiToken}&organization=${codEntidad}`;
        const endpoint = environment.userListOrg + params;

        return this._api.get(endpoint);
    }

    //
    /*obtenerRoles (roles: rolesDTO ): Observable<any> {
        const endpoint = environment.roles ;
        return this._api.get(endpoint);

    }*/

    /*crearUsuario(user: any): Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.put(endpoint, user);
    }*/

    editarUsuario(user: any): Observable<any> {
        const endpoint = environment.user_update;
        return this._api.put(endpoint, JSON.stringify(user));
    }

    eliminarUsuario(user: any): Observable<any> {
        const endpoint = environment.user_endPoint + '/user';
        return this._api.delete(endpoint, user);
    }

    getEntidades() {

        const endpoint = environment.getEntidades;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});

    }
}
