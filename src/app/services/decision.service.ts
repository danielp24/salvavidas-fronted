import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { ProcessRequestDTO } from "../modelo/dto/processRequestDTO";
import { Observable } from "rxjs";
import { ProcessResponseDTO } from "../modelo/dto/processResponseDTO";
import { environment } from "src/environments/environment";

@Injectable()
export class DecisionService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarReglas (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.regla_endPoint + '/rules';
        return this._api.post(endpoint, request);
    }

    evaluarRegla (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.regla_endPoint + '/evaluateRule';
        return this._api.post(endpoint, request);
    } 
}