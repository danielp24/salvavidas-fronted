import {Injectable} from "@angular/core";
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {DeshabilitarParametroDTO} from "../modelo/dto/DeshabilitarParametroDTO";
import {BodyParametroDTO} from "../modelo/dto/BodyParametroDTO";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {GetListParametrosGeneralDTO} from "../modelo/dto/getListParametrosGeneralDTO";

@Injectable({
    providedIn: 'root'
})
export class ParametrosService {
    authorization: string;

    constructor(private _api: ApiManagerService) {

        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }


    getParametros(codParametro: any): Observable<any> {

        const endpoint = environment.listaParametros + codParametro;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});


    }

    getParametrosIdDOminio(codDominio: GetListParametrosGeneralDTO) {

        const endpoint = environment.getListParameter;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: codDominio, headers: headers});
    }

    deshabilitarParametro(idParametro: DeshabilitarParametroDTO) {
        const endpoint = environment.deshabilitarParametro;
        const parametros = idParametro;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.put(endpoint, null, {body: parametros, headers: headers});
    }

    actualizarParametro(parametro: BodyParametroDTO) {
        const endpoiint = environment.actualizarParametro;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.put(endpoiint, null, {body: parametro, headers: headers});
    }

    agregarParametro(nuevoParametro: BodyParametroDTO) {
        const endpoint = environment.agregarParametro;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: nuevoParametro, headers: headers});

    }
}
