import { Injectable } from "@angular/core";
import { ApiManagerService } from "../infraestructura/api/api-manager";
import { ProcessRequestDTO } from "../modelo/dto/processRequestDTO";
import { Observable } from "rxjs";
import { ProcessResponseDTO } from "../modelo/dto/processResponseDTO";
import { environment } from "src/environments/environment";


@Injectable()
export class TareaService {

    constructor(
        private _api: ApiManagerService
    ) {}

    consultarTareasPorInstancia (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/tasksByProcessInstance';
        return this._api.post(endpoint, request);
    }

    consultarTareasPorGrupos (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/tasksByGroups';
        console.log("request tareas por grupos: "+ JSON.stringify(request));
        return this._api.post(endpoint, request);

    }

    cambiarEstadoTarea (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/taskStatus';
        return this._api.put(endpoint, request);
    }

    reasignarTarea (request: ProcessRequestDTO) : Observable<ProcessResponseDTO> {
        const endpoint = environment.tarea_endPoint + '/reassignTask';
        return this._api.put(endpoint, request);
    }

}
