import {Injectable} from "@angular/core";
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {RequestPagosDTO} from "../modelo/dto/pse/RequestPagosDTO";
import {environment} from "../../environments/environment";
import {RequestActualizarPagoDTO} from "../modelo/dto/pse/RequestActualizarPagoDTO";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {headerDTO} from "../modelo/dto/common/domain/generic/user/headerDTO";


@Injectable({
    providedIn: 'root'
})
export class PagosService {
    authorization: string;

    constructor(
        private _api: ApiManagerService
    ) {

        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }

    insertarPago(newPay: RequestPagosDTO) {
        const endpoint = environment.insertarPago;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: newPay, headers: headers});
    }

}

