import {Injectable} from '@angular/core';
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {environment} from "../../environments/environment";
import {changedPasswordDTO} from "../modelo/dto/changedPasswordDTO";
import {headerDTO} from "../modelo/dto/common/domain/generic/user/headerDTO";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class ChangedPassService {

    constructor(private _api: ApiManagerService,
                private http: HttpClient) {
    }


    changedPassword(change: changedPasswordDTO):Observable<any> {
        const endpoint = environment.changedPassword;
        return this.http.post(endpoint, change)
    }
    policesNewPassword(header: headerDTO){
        const endpoint = environment.policesNewPassword;
        return this._api.post(endpoint, header);
    }


}
