import {VerificacionPagoDTO} from "../modelo/dto/pse/VerificacionPagoDTO";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";


@Injectable({
    providedIn: 'root'
})

export class VerificarPagoService {
    constructor(private http: HttpClient) {
    }

    verificarPago(datosVerificacion: VerificacionPagoDTO) {

        const endpoint = environment.iniciarVerificacionPago;
        return this.http.post(endpoint, datosVerificacion);


    }
}
