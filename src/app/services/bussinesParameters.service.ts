import {BusinessParametersDTO} from "../modelo/dto/common/domain/generic/bussines/BusinessParametersDTO";
import {UserDTO} from "../modelo/dto/userDTO";
import {BasicPagoDTO} from "../modelo/dto/common/domain/generic/bussines/BasicPagoDTO";
import {TokenDTO} from "../modelo/dto/TokenDTO";
import {headerDTO} from "../modelo/dto/common/domain/generic/user/headerDTO";
import {BodyDTO} from "../modelo/dto/common/domain/generic/user/bodyDTO";
import {RolesDTO} from "../modelo/dto/rolesDTO";

export class BussinesParameterService {

    private businessParameter: BusinessParametersDTO = new BusinessParametersDTO();


    constructor() {
    }

    BussinesParameterService() {

    }

    homologarData() {
        //Homologando Current User
        const currentUser = new UserDTO();
        currentUser.header = new headerDTO();
        currentUser.body = new BodyDTO();



        // currentUser.body = (JSON.parse(localStorage.getItem('currentUser')) != null) ? JSON.parse(localStorage.getItem('curretnUser')) : new BodyDTO();
        this.businessParameter.currentUser = (JSON.parse(sessionStorage.getItem('currentUser')) != null) ? JSON.parse(sessionStorage.getItem('currentUser')) : new UserDTO();
       // this.businessParameter.currentUser = currentUser;
        this.businessParameter.basic = (JSON.parse(sessionStorage.getItem('basic')) != null) ? JSON.parse(sessionStorage.getItem('basic')) : new BasicPagoDTO();
        console.log("Current User, enrutador: " + JSON.stringify(this.businessParameter.currentUser));
     //   console.log("datos basicos entidad, enrutador: " + JSON.stringify(this.businessParameter.basic));
    }

    getBussinessParams() {
        return this.businessParameter;
    }
}
