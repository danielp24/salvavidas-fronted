import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ApiManagerService} from 'src/app/infraestructura/api/api-manager';
import {environment} from 'src/environments/environment';
import {UserDTO} from '../modelo/dto/userDTO';
import {LogOutDTO} from "../modelo/dto/LogOutDTO";
import {apiUser, recaptcha, statesApiUser} from 'src/environments/environment.variables';
import {headerDTO} from '../modelo/dto/common/domain/generic/user/headerDTO';
import {BodyNoPassDTO} from '../modelo/dto/common/domain/generic/user/bodyNoPassDTO';
import {RolesDTO} from "../modelo/dto/rolesDTO";
import {OwnerUserDTO} from "../modelo/dto/OwnerUserDTO";
import {ReestablecerContraseñaDTO} from "../modelo/dto/ReestablecerContraseña.DTO";
import {RecaptchaDTO} from "../modelo/so/RecaptchaDTO";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class LoginService {

    private currentUserSubject: BehaviorSubject<UserDTO>;
    public currentUser: Observable<UserDTO>;
    public current: UserDTO;
    private roles: RolesDTO;
    valorRol: string;


    constructor(
        private _api: ApiManagerService,
        private api: HttpClient
    ) {
        this.currentUserSubject = new BehaviorSubject<UserDTO>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserDTO {
        return this.currentUserSubject.value;
        console.log(this.currentUserValue);
    }

    autenticar(user: OwnerUserDTO):Observable<any> {
        const endpoint = environment.autenticar;
        return this.api.post(endpoint, user);
    }

    login(userDto: UserDTO): Observable<UserDTO> {
        const endpoint = environment.user_endPoint;
        const arregloRoles: RolesDTO[] = [];

        return this._api.post(endpoint, JSON.stringify(userDto))
            .pipe(map(data => {
                if (!!data.body && data.status === statesApiUser.ok) {
                    const usuario = userDto;
                    usuario.body.id = data.body.body.id;
                    usuario.header.apiToken = data.body.body.token;
                    usuario.body.name = data.body.body.name;
                    usuario.body.email = data.body.body.email;
                    usuario.body.phoneNumber = data.body.body.phoneNumber;
                    usuario.body.organization = data.body.body.organization;
                    for (var rol in data.body.body.roles) {
                        this.roles = JSON.parse(JSON.stringify(data.body.body.roles[rol]));
                        arregloRoles.push(this.roles)
                    }

                    usuario.body.roles = arregloRoles;
                    sessionStorage.removeItem('currentUser');
                    sessionStorage.setItem('currentUser', JSON.stringify(usuario));

                    this.currentUserSubject.next(usuario);

                    this.valorRol = this.roles.description;
                    return data;
                } else {
                    return null;
                }
            }));
    }


    currentRol() {
        //metodo para constrolar el rol del usua
        this.current = JSON.parse(sessionStorage.getItem('currentUser'));
        for (var rol in this.current.body.roles) {
            this.roles = JSON.parse(JSON.stringify(this.current.body.roles[rol]));
        }
        this.valorRol = this.roles.description;
        return this.valorRol;

    }

    logout() {
        const userLogout = new LogOutDTO();
        const userToken = JSON.parse(sessionStorage.getItem('currentUser'));
        userLogout.header = new headerDTO;
        userLogout.body = new BodyNoPassDTO;
        userLogout.header.apiToken = apiUser.tokenApplication;
        userLogout.body.userToken = "";

        return this._api.post(environment.userLogout_endPoint, JSON.stringify(userLogout))
            .pipe(map(data => {
                if (data.status === statesApiUser.ok) {
                    this.currentUserSubject.next(null);
                } else {
                    return null;
                }
            }));
    }

    recoverPassword(userDto: UserDTO) {
        const endpoint = environment.userRecovery;
        return this._api.put(endpoint, userDto)
    }

    notificarRestablecerCorreo(correo: ReestablecerContraseñaDTO):Observable<any> {
        const endpoint = environment.recoveryPassword;
        return this.api.post(endpoint, correo);
    }

    validarToken(token: string) {
        const endpoint = environment.verificarTokenPassword + token;

        return this._api.post(endpoint);
    }

    verificarRecaptcha(response: string, secretKey: string) {
        const endpoint = environment.recaptcha;
        const body = {
            response: response,
            secret: secretKey
        }

        return this._api.post(endpoint, null , {body:body} )

    }

}
