import {Injectable} from "@angular/core";
import {ApiManagerService} from "../infraestructura/api/api-manager";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {HttpHeaders} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class DptoService {
    authorization: string;

    constructor(private _api: ApiManagerService) {
        this.obtenerBusinessToken();
    }

    obtenerBusinessToken() {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));

        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }

    getList() {

        const endpoint = environment.listaDepartamentos;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});

    }

    getMunicipios(idDpto: number): Observable<any> {

        const endpoint = environment.getMunicipios + idDpto;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});

    }

}
