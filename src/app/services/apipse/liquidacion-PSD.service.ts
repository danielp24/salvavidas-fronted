import {Injectable} from "@angular/core";
import {LiquidacionPsdDTO} from "../../modelo/dto/pse/LiquidacionPsdDTO";
import {HttpHeaders} from "@angular/common/http";
import {ApiManagerService} from "../../infraestructura/api/api-manager";
import {environment} from "../../../environments/environment";
import {BasicPagoDTO} from "../../modelo/dto/common/domain/generic/bussines/BasicPagoDTO";


@Injectable({
    providedIn: 'root',
})
export class LiquidacionPSDService {
    authorization: string;
    private dataLiquidacion: LiquidacionPsdDTO[] = [
        {
            vr_total_a_pagar: 34000,
            vr_base_psd: 2300,
            vr_depositos: 12000,
            posicion_neta_val: 123,
            saldo_a_favor_antes: 123455,
            saldo_a_cargo: 5000,
            interes_Mora: 23455,
            dias_Mora: 2,
            tasa_Mora: 2000000
        },
        {
            vr_total_a_pagar: 7000,
            vr_base_psd: 200,
            vr_depositos: 2000,
            posicion_neta_val: 1,
            saldo_a_favor_antes: 44444,
            saldo_a_cargo: 3000,
            interes_Mora: 1455,
            dias_Mora: 10,
            tasa_Mora: 8000
        }
    ];

    constructor(
        private _api: ApiManagerService) {

        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }

    getLiquidacionPSD() {
        return this.dataLiquidacion;
    }

    getFechaCorte() {

        const endpoint = environment.getFechaCorte;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});


    }

    getDetalleLiquidacion(RequestDetalleLiquidacion: BasicPagoDTO) {
        const endpoint = environment.getDetalleLiquidacion;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: RequestDetalleLiquidacion, headers: headers});
    }

    /*  getPeriodo(){
          const endpoint = environment.getPeriodo;
          const headers = new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.authorization
          });
          return this._api.get(endpoint, null, {headers:headers});
      }*/
    getBasesLiquidacion(reequestgetBasesLiquidacion: BasicPagoDTO) {
        const endpoint = environment.getBasesLiquidacion;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: reequestgetBasesLiquidacion, headers: headers});
    }

    getSaldoTotalDepositos(reequestgetBasesLiquidacion: BasicPagoDTO) {
        const endpoint = environment.getTotalSaldoDepositos;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: reequestgetBasesLiquidacion, headers: headers});
    }

    getDetalleRetransmision(reequestgetBasesLiquidacion: BasicPagoDTO) {
        const endpoint = environment.getDetalleRetransmision;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: reequestgetBasesLiquidacion, headers: headers});


    }

    getSaldosPendientes(reequestgetBasesLiquidacion: BasicPagoDTO) {
        const endpoint = environment.getSaldosPendientes;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: reequestgetBasesLiquidacion, headers: headers});

    }
}

