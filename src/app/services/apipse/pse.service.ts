import {RequestCreateTransactionDTO} from "../../modelo/dto/pse/RequestCreateTransactionDTO";
import {ResponseCreateTransactionDTO} from "../../modelo/dto/pse/ResponseCreateTransactionDTO";
import {Observable} from "rxjs";
import {ApiManagerService} from "../../infraestructura/api/api-manager";
import {environment} from "../../../environments/environment";
import {Injectable} from "@angular/core";
import {apiPse} from "../../../environments/environment.variables";
import {TicketIdActual} from "../../modelo/dto/pse/ticketIdActual";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PseService {
    ticketId: string;

    constructor(private api: ApiManagerService, private _api: HttpClient) {
    }

    createTransaction(transaction: RequestCreateTransactionDTO): Observable<any> {
        const endpoint = environment.createTransaction;
        console.log(transaction);
        return this._api.post(endpoint, transaction);


    }

    getTransaction(ticket: string): Observable<any> {
        const endpoint = environment.getTransaction + `?entityCode=${apiPse.entityCode}&ticketId=${ticket}`;
        return this._api.get(endpoint);


    }
}

