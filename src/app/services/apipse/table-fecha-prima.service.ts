import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {TablaFechaPrimaDTO} from "../../modelo/dto/pse/TablaFechaPrimaDTO";

@Injectable({
    providedIn: 'root'
})
export class TableFechaPrimaService {

    private tableFechaPrima: TablaFechaPrimaDTO[] = [
        {
            fechaInicio: "29-05-18",
            fechaFinal: "29-05-19",
            tipoPrima: "Normal",
            valorSobrePrima: 700000,
            porcentaje: 0.05,
        },
        {
            fechaInicio: "17-04-18",
            fechaFinal: "22-01-19",
            tipoPrima: "Riesgo Junta",
            valorSobrePrima: 2100000,
            porcentaje: 0.3,
        },
    ];

    constructor(private http: HttpClient) { }

    getTableFechaPrima() {
        return this.tableFechaPrima;
    }
}
