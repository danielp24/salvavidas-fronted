import {Injectable} from "@angular/core";
import {ApiManagerService} from "../../infraestructura/api/api-manager";

import {RequestDetallePagoDTO} from "../../modelo/dto/pse/RequestDetallePagoDTO";
import {environment} from "../../../environments/environment";
import {RequestActualizarPagoDTO} from "../../modelo/dto/pse/RequestActualizarPagoDTO";
import {HttpHeaders} from "@angular/common/http";


@Injectable({
    providedIn: 'root'
})
export class ConsultarDetallePagosService {
    authorization: string;

    constructor(
        private _api: ApiManagerService,
    ) {
        const jsonSessionStorage = JSON.parse(sessionStorage.getItem('currentUser'));
        this.authorization = jsonSessionStorage.header.tokenBusiness;
    }

    actualizarPago(detallePagoAct: RequestActualizarPagoDTO) {
        const endpoint = environment.actualizarPago;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.put(endpoint, null, {body:detallePagoAct, headers: headers});
    }

    consultarDetallePagosTrimestre(detallesPago: RequestDetallePagoDTO) {

        const endpoint = environment.consultarDetallePagos;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body: detallesPago, headers: headers});


    }

    consultarDetalleUltimoPago(detallesPago: RequestDetallePagoDTO) {
        const endpoint = environment.consultarDetallePagos;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.post(endpoint, null, {body:detallesPago,headers: headers});

    }

    getEntidadByCode(code: string) {

        const endpoint = environment.obtenerCooperativaByCod + code;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authorization
        });
        return this._api.get(endpoint, null, {headers: headers});


    }

}
