import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {AccordionModule} from 'primeng/accordion';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ButtonModule} from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import {CardModule} from 'primeng/card';
import {CarouselModule} from 'primeng/carousel';
import {ChartModule} from 'primeng/chart';
import {CheckboxModule} from 'primeng/checkbox';
import {ChipsModule} from 'primeng/chips';
import {CodeHighlighterModule} from 'primeng/codehighlighter';
import {ColorPickerModule} from 'primeng/colorpicker';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DataViewModule} from 'primeng/dataview';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {EditorModule} from 'primeng/editor';
import {FieldsetModule} from 'primeng/fieldset';
import {FileUploadModule} from 'primeng/fileupload';
import {GalleriaModule} from 'primeng/galleria';
import {GrowlModule} from 'primeng/growl';
import {InplaceModule} from 'primeng/inplace';
import {InputMaskModule} from 'primeng/inputmask';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {LightboxModule} from 'primeng/lightbox';
import {ListboxModule} from 'primeng/listbox';
import {MegaMenuModule} from 'primeng/megamenu';
import {MenuModule} from 'primeng/menu';
import {MenubarModule} from 'primeng/menubar';
import {MessageModule} from 'primeng/message';
import {MessagesModule} from 'primeng/messages';
import {MultiSelectModule} from 'primeng/multiselect';
import {NgxExtendedPdfViewerModule} from 'ngx-extended-pdf-viewer';
import {NgxPrintModule} from 'ngx-print'
import {OrderListModule} from 'primeng/orderlist';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {PaginatorModule} from 'primeng/paginator';
import {PanelModule} from 'primeng/panel';
import {PanelMenuModule} from 'primeng/panelmenu';
import {PasswordModule} from 'primeng/password';
import {PickListModule} from 'primeng/picklist';
import {BlockUIModule, ConfirmationService, DataTableModule} from 'primeng/primeng';
import {ProgressBarModule} from 'primeng/progressbar';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {RadioButtonModule} from 'primeng/radiobutton';
import {RatingModule} from 'primeng/rating';
import {ScheduleModule} from 'primeng/schedule';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {SelectButtonModule} from 'primeng/selectbutton';
import {SlideMenuModule} from 'primeng/slidemenu';
import {SliderModule} from 'primeng/slider';
import {SplitButtonModule} from 'primeng/splitbutton';
import {StepsModule} from 'primeng/steps';
import {TableModule} from 'primeng/table';
import {TabMenuModule} from 'primeng/tabmenu';
import {TabViewModule} from 'primeng/tabview';
import {TerminalModule} from 'primeng/terminal';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {ToastModule} from 'primeng/toast';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {ToolbarModule} from 'primeng/toolbar';
import {TooltipModule} from 'primeng/tooltip';
import {TreeModule} from 'primeng/tree';
import {TreeTableModule} from 'primeng/treetable';
import {AppComponent} from './app.component';
import {CaptchaModule} from 'primeng/captcha';

// PrimeNG
import {AppRoutes} from './app.routes';
import {MessageService} from "primeng/api";
// Información Proceso
import {HttpConfigInterceptor} from './infraestructura/api/api-interceptor';
import {AdminLayoutComponent} from './infraestructura/layout-components/adminLayoutComponent/adminLayout/adminLayout.component';
import {AppBreadcrumbComponent} from './infraestructura/layout-components/adminLayoutComponent/breadcrumb/app.breadcrumb.component';
import {BreadcrumbService} from './infraestructura/layout-components/adminLayoutComponent/breadcrumb/breadcrumb.service';
import {AppFooterComponent} from './infraestructura/layout-components/adminLayoutComponent/footer/app.footer.component';
import {AppMenuComponent} from './infraestructura/layout-components/adminLayoutComponent/menu/app.menu.component';
import {AppSubMenuComponent} from './infraestructura/layout-components/adminLayoutComponent/menu/subMenu/app.SubMenu.Component';
import {AppInlineProfileComponent} from './infraestructura/layout-components/adminLayoutComponent/profile/app.profile.component';
import {AppRightpanelComponent} from './infraestructura/layout-components/adminLayoutComponent/rightpanel/app.rightpanel.component';
import {AppTopbarComponent} from './infraestructura/layout-components/adminLayoutComponent/topbar/app.topbar.component';
import {AlertComponent} from './infraestructura/layout-components/alertComponent/alert.component';
import {AlertService} from './infraestructura/utils/alert.service';
import {EnrutadorPantallasComponent} from './pantallas-proceso/enrutador-pantallas/enrutador-pantallas.component';
// Infraestructura
import {RESOURCES} from './infraestructura/utils/extensions';
import {LoadingService} from './infraestructura/utils/loading.service';
import {AdministracionProcesosComponent} from './pantallas-proceso/administracionProcesos/administracionProcesos.component';
import {AdministracionTareasComponent} from './pantallas-proceso/administracionTareas/administracionTareas.component';
import {ReasignarTareaComponent} from './pantallas-proceso/administracionTareas/reasignarTarea/reasignarTarea.component';
import {ReasignarTareaDetalleComponent} from './pantallas-proceso/administracionTareas/reasignarTarea/reasignarTareaDetalle/reasignarTareaDetalle.component';
import {AdministracionUsuariosComponent} from './pantallas-proceso/administracionUsuarios/administracionUsuarios.component';
import {AdministracionParametrosComponent} from './pantallas-proceso/administracionParametros/administracionParametros.component';
import {DetalleAdministracionUsuariosComponent} from './pantallas-proceso/administracionUsuarios/detalleAdministracionUsuarios/detalleAdministracionUsuarios.component';
import {PerfilUsuarioComponent} from './pantallas-proceso/administracionUsuarios/perfilUsuario.component/perfilUsuario.component';
import {CabeceraProcesoComponent} from './pantallas-proceso/componentes/cabeceraProceso/cabeceraProceso.component';
import {DatosContactoComponent} from './pantallas-proceso/componentes/datosContacto/datosContacto.component';
import {InformacionGeneralComponent} from './pantallas-proceso/componentes/informacion-general/informacion-general.component';
import {DashboardComponent} from './pantallas-proceso/dashboard/dashboard.component';
import {LoginModComponent} from './pantallas-proceso/login-mod/login-mod.component';
import {AuthGuardService} from './services/authGuard.service ';
import {ContenedorService} from './services/contenedor.service';
import {DecisionService} from './services/decision.service';
import {LoginService} from './services/login.service';
import {ProcesoService} from './services/proceso.service';
import {TareaService} from './services/tarea.service';
import {UsuarioService} from './services/usuario.service';
import {ChangePasswordComponent} from './pantallas-proceso/privacidad/change-password/change-password.component';
import {ManagerGuard} from "./infraestructura/shared/manager.guard";
import {GestionPSDComponent} from './pantallas-proceso/gestion-psd/gestion-psd.component';
import {AbonoDevolucionComponent} from './pantallas-proceso/abono-devolucion/abono-devolucion.component';
import {TablaCalculoPrimasComponent} from './pantallas-proceso/tablas/tabla-calculo-primas/tabla-calculo-primas.component';
import {TablaFechaPrimasComponent} from './pantallas-proceso/tablas/tabla-fecha-primas/tabla-fecha-primas.component';
import {InfoPagoComponent} from './pantallas-proceso/info-pago/info-pago.component';
import {ConfirmacionPagoComponent} from './pantallas-proceso/confirmacion-pago/confirmacion-pago.component';
import {PseService} from "./services/apipse/pse.service";
import {RecoverPasswordComponent} from './pantallas-proceso/recover-password/recover-password.component';
import {ListaPagosComponent} from './pantallas-proceso/tablas/lista-pagos/lista-pagos.component';
import {onlyNumberDecimal} from "./infraestructura/utils/onlyNumberDecimal";
import {LoaderComponent} from './infraestructura/utils/loader/loader.component';

//Pipes
import {ThousandsPipe} from './infraestructura/utils/pipes/ThousandsPipe';
import {DetalleAdministracionParametrosComponent} from './pantallas-proceso/administracionParametros/detalle-administracion-parametros/detalle-administracion-parametros.component';
import {SpinnerModule} from "@uiowa/spinner";
import { LoaderSpinnerComponent } from './infraestructura/utils/loader-spinner/loader-spinner.component';
import {MessagesAlertTimeFadeOut} from "./infraestructura/utils/messagesAlertTimeFadeOut/messages-alert-time-fade-out.component";
import { WelcomeUserComponent } from './pantallas-proceso/welcome-user/welcome-user.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutes,
        HttpClientModule,
        BrowserAnimationsModule,
        AccordionModule,
        AutoCompleteModule,
        BlockUIModule,
        BreadcrumbModule,
        ButtonModule,
        CalendarModule,
        CardModule,
        CarouselModule,
        ChartModule,
        CheckboxModule,
        ChipsModule,
        CodeHighlighterModule,
        ConfirmDialogModule,
        ColorPickerModule,
        ContextMenuModule,
        DataTableModule,
        DataViewModule,
        DialogModule,
        DropdownModule,
        EditorModule,
        FieldsetModule,
        FileUploadModule,
        GalleriaModule,
        GrowlModule,
        InplaceModule,
        InputMaskModule,
        InputSwitchModule,
        InputTextModule,
        InputTextareaModule,
        LightboxModule,
        ListboxModule,
        MegaMenuModule,
        MenuModule,
        MenubarModule,
        MessageModule,
        MessagesModule,
        MultiSelectModule,
        NgxExtendedPdfViewerModule,
        NgxPrintModule,
        OrderListModule,
        OrganizationChartModule,
        OverlayPanelModule,
        PdfViewerModule,
        PaginatorModule,
        PanelModule,
        PanelMenuModule,
        PasswordModule,
        PickListModule,
        ProgressBarModule,
        ProgressSpinnerModule,
        RadioButtonModule,
        RatingModule,
        ScheduleModule,
        ScrollPanelModule,
        SelectButtonModule,
        SlideMenuModule,
        SliderModule,
        SpinnerModule,
        SplitButtonModule,
        StepsModule,
        TableModule,
        TabMenuModule,
        TabViewModule,
        TerminalModule,
        TieredMenuModule,
        ToastModule,
        ToggleButtonModule,
        ToolbarModule,
        TooltipModule,
        TreeModule,
        TreeTableModule,
        CaptchaModule,
        SpinnerModule

    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        ThousandsPipe,

    ],
    declarations: [
        AppComponent,
        AppMenuComponent,
        AppSubMenuComponent,
        AppTopbarComponent,
        AppFooterComponent,
        AppBreadcrumbComponent,
        AppRightpanelComponent,
        AppInlineProfileComponent,
        InformacionGeneralComponent,
        AdminLayoutComponent,
        DashboardComponent,
        AlertComponent,
        LoginModComponent,
        AdministracionUsuariosComponent,
        DetalleAdministracionUsuariosComponent,
        AdministracionParametrosComponent,
        PerfilUsuarioComponent,
        AdministracionProcesosComponent,
        AdministracionTareasComponent,
        ReasignarTareaDetalleComponent,
        ReasignarTareaComponent,
        DatosContactoComponent,
        CabeceraProcesoComponent,
        ChangePasswordComponent,
        GestionPSDComponent,
        AbonoDevolucionComponent,
        EnrutadorPantallasComponent,
        TablaCalculoPrimasComponent,
        TablaFechaPrimasComponent,
        InfoPagoComponent,
        ConfirmacionPagoComponent,
        RecoverPasswordComponent,
        ListaPagosComponent,
        onlyNumberDecimal,
        LoaderComponent,
        ThousandsPipe,
        DetalleAdministracionParametrosComponent,
        LoaderSpinnerComponent,
        MessagesAlertTimeFadeOut,
        WelcomeUserComponent



    ],
    providers: [
        // {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
        BreadcrumbService,
        ConfirmationService,
        AuthGuardService,
        LoginService,
        AlertService,
        LoadingService,
        ContenedorService,
        ProcesoService,
        TareaService,
        UsuarioService,
        DecisionService,
        MessageService,
        RESOURCES,
        ManagerGuard,
        PseService

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
