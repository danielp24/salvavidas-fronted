import {ADMINISTRADOR, CONSULTA, COOPERATIVA, PROFESIONAL_TESORERO, USUARIO} from "./app.roles";


export const ROUTES_PATH = {

    home: 'home',
    login: 'login',
    recuperarContraseña: 'recuperarContrasena',
    dashboard: {url: 'dashboard' ,rol:  [ADMINISTRADOR, USUARIO]},
    adminUser: 'adminUser',
    adminParameter: 'adminParameter',
    updateData: {url:'actualizacionDatos', rol:[ADMINISTRADOR]},
    adminPlantilla: {url:'administracionPlantillas', rol:[ADMINISTRADOR]},
    privacidad: 'privacidad',
    userProfile: 'userProfile',
    processes: {url: 'processes', rol: [ADMINISTRADOR]},
    task: {url: 'tasks', rol: [USUARIO]},
    reassignTask: {url: 'reassignTask', rol: [USUARIO]},
    enrutadorPantalla: {url: 'enrutadorPantalla', rol:  [ADMINISTRADOR, COOPERATIVA, USUARIO, PROFESIONAL_TESORERO, CONSULTA]},
    registrarPagoConsignacion: {url: 'rPagoConsignacion', rol: [PROFESIONAL_TESORERO]},
    informacionPagoPSE: {url: 'infoPago', rol: [COOPERATIVA]},
    confirmarPago: {url: 'confirmacionPago', rol: [COOPERATIVA]},
    consultaCooperativa: {url: 'consultaMovimientosCooperativa', rol:  [COOPERATIVA]},
    consultaFogacoop: {url: 'consultaMovimientosCooperativaFogacoop', rol:  [ADMINISTRADOR, PROFESIONAL_TESORERO, CONSULTA]},
    informacionPagoNoLiquidado: {url: 'infoPagoNoLiquidado', rol:  [ADMINISTRADOR]},
    gestionPagosPsd: {url: 'GestionarpagosPSD', rol:  [USUARIO]},
    abonoDevolucion: {url: 'Confirmarabonodevolucion', rol:  [USUARIO]},
    reportes: {url:'reportes', rol: [CONSULTA]},
    welcomeUser:{url:'welcomeUser', rol: [COOPERATIVA]}


};
