import {environment} from './environment.prod';

export const states = {
    create: 'Created',
    ready: 'Ready',
    reserved: 'Reserved',
    inProgress: 'InProgress',
    suspended: 'Suspended',
    completed: 'Completed',
    failed: 'Failed',
    error: 'Error',
    exited: 'Exited',
    obsolete: 'Obsolete'
}

export const statesTaskActions = {
    claimed: 'claimed',
    released: 'released',
    delegated: 'delegated',
    completed: 'completed',
    activated: 'activated',
    forwarded: 'forwarded',
    nominated: 'nominated',
    exited: 'exited',
    started: 'started',
    stopped: 'stopped',
    suspended: 'suspended',
    resumed: 'resumed',
    failed: 'failed',
    skipped: 'skipped'
}

export const successResponse = {
    type: 'SUCCESS',
    statusCode: '200'
}

export const createdResponse = {
    type: 'SUCCESS',
    statusCode: '201'
}

export const userAuthenticationSuccessful = {
    type: 'SUCCESS',
    msg: 'Authentication successful',
    statusCode: '200'
}

export const failUserNotFound = {
    type: 'FAIL',
    msg: 'User not found',
    statusCode: '404'
}

export const failUserNotAuthorized = {
    type: 'FAIL',
    msg: 'User Not Authorized',
    statusCode: '204'
}

export const failureType = {
    error: 'ERROR',
    fail: 'FAIL',
    ok: 'OK',
    cero: '0'

}

export const failDemandNotFound = {
    type: 'ERROR',
    msg: 'Demand not found',
    statusCode: '404'
}

export const statesApiUser = {
    ok: 'OK'
}

export const apiPse = {
    entityCode: '10396',
    srvCode: '1001',
    urlRedirect: 'portal-toolbox/#/confirmacionPago',


}

export const apiUser = {
    tokenApplication: '95d6aa45-b29e-489a-a8ea-ae1ee20e8de1',
    userOrganization: 'SOAINT',
    msgServiceListOrg: 'No se han retornado elementos en la lista'
}


export const statesPagos = {
    CREATED: {codigo: "0", nombre: "CREATED", display: "Creada"},
    PENDING: {codigo: "1", nombre: "PENDING", display: "Pendiente"},
    OK: {codigo: "2", nombre: "OK", display: "Aprobada"},
    FAILED: {codigo: "3", nombre: "FAILED", display: "Fallida"},
    NOT_AUTHORIZED: {codigo: "4", nombre: "NOT_AUTHORIZED", display: "Rechazada"},
    NO_LIQUIDADO: {codigo: "5", nombre: "NO_LIQUIDADO", display: "Creada"},
    NO_COMPROBANTE: {codigo: "6", nombre: "NO_COMPROBANTE", display: "Aprobada"},
    EXITOSO: {codigo: "7", nombre: "EXITOSO", display: "Aprobada"},
    EXPIRED: {codigo: "8", nombre: "EXPIRED", display: "Expirado"},

}

export const gestionPagosPse = {
    CodOrigenPagoPSE: '1',
    estadolistarHistoricoPagos: 'NA',
    codBancoDestino: '13',
    estadoPagoInicial: '0'
}
export const table = {
    maxRowsPage: '5'
}
export const gestionCodEntidad = {
    ENTIDAD: {nombreEntidad: "FOGACOOP", codigoEntidad: ""}
}
export const listaParametros = {
    dominioActivo: {codigo: "1", nombreParametro: "Activos"},
    dominioNoActivo: {codigo: "0", nombreParametro: "Inactivos"},
    parametrosGeneral: {codigo: "2", nombreParametro: "Activos/Inactivos"},
    idPadreNulo: {value: "0", name: "No Aplica"}

}
export const recuperarContraseña = {
    usuarioAdmin: {password: "NewPasswordUser4*", username: "pruebas4"}
}
export const estado_NoAutorizado = {
    noAutorizado: {code: "401", value: "unauthorized"}
}
export const statusBussines = {
    OK: 'OK',
    BAD_REQUEST: {nombre: 'BAD_REQUEST', codigo: 400}
}

export const statusSesion = {
    contraseñaIncorrecta: {nombre: 'contraseña incorrecta', codigo: 1}
}

export const timeComponent = {
    timeFadeOutMessageComponent: 10
}
export const messageValorSugerido = {
    noValores: {valor: 'Sin información sugerida de pago por encontrarse en procesamiento o sin reporte de la misma'}
}
export const caracterEspecial = {
    numeros: {valor: '.0123456789'}
}
export const estadoEntidad = {
    preliquidada: {codigo: 1}
}
export const dominioPaginaBienvenida = {
    dominio: {idDominio: 36, disabled: 0}
}
export const errorApiUSer = {
    BadRequest: {code: '3008'}
}
export const cuerpoNoticia = {
    titulo: {name: 'titulo', subtitulo: 'subtitulo', cuerpoMensaje: 'cuerpoMensaje'}
}
//////////////////////////// CLAVES RECAPTCHA AMBIENTE QA Y LOCALHOST ///////////////////////

/*export const recaptcha = {
    secret: '6LcsWJsUAAAAAPKm7QaPzmzoikU7Y2QkgAhpbp3U',
    site_key: '6LcsWJsUAAAAADU-BtDcUWSyNqKhPAkYoz4LsIZ6'

}*/

//////////////////////////// CLAVES RECAPTCHA AMBIENTE QA ///////////////////////


//////////////////////////CLAVES RECAPTCHA AMBIENTE SOAINT//////////////////////


export const recaptcha = {
    secret: '6Lek9LYUAAAAABTVKfWc3JPBMS0WcjBTYkbJlTn0',
    site_key: '6Lek9LYUAAAAANKyWkWBl9cGcpLm0iEG3wlCpTGs'
}
//////////////////////////CLAVES RECAPTCHA AMBIENTE SOAINT//////////////////////
