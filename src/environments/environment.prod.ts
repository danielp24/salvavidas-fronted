// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
////////////////////////////// DESARROLLO SOAINT /////////////////////////////
const urlEnviroment = 'http://181.49.173.42:28080/';
const urlApiServer = 'http://181.49.173.42:28080/';

////////////////////////////// DESARROLLO SOAINT /////////////////////////////

////////////////////////////// QA FOGACOOP   /////////////////////////////

/*const urlEnviroment='http://pruebaswebtoolbox:8080/';
const urlApiServer='http://pruebaswebnegocio:8080/';*/

//////////////////////////////} QA FOGACOOP   /////////////////////////////

const apiJbpm = urlApiServer + 'ApiJbpm/api/';
const apiNegocio = urlApiServer + 'ApiNegocio/fogacoop/';
const apiReglas = urlApiServer + 'ApiReglas/';
const apiUser = urlApiServer + 'ApiUser/fogacoop/';
const apiPse = urlApiServer + 'ApiPse/fogacoop/pse/';
const apiSgd = urlApiServer + 'ApiSgd/';
const apiOrquestacion = urlApiServer + 'ApiOrquestacion/fogacoop/';


export const environment = {
    production: true,
    //UrlReviroment
    urlEnviroment: `${urlEnviroment}`,
    //Iframe Path / Ruta de Iframes
    iframesPath: `${urlEnviroment}fogaweb/#/`,
    //iframesPath: `${urlEnviroment}portal-toolbox/#/`
    // Api BPM
    contenedor_endPoint: `${apiJbpm}container`,
    proceso_endPoint: `${apiJbpm}process`,
    tarea_endPoint: `${apiJbpm}task`,
    regla_endPoint: `${apiJbpm}rule`,
    // ApiUser
    //Accounts
    user_endPoint: `${apiUser}login`,
    userLogout_endPoint: `${apiUser}logout`,
    userRecovery: `${apiUser}recovery-pass`,
    changedPassword: `${apiUser}changed`,
    user_update: `${apiUser}update`,
    policesNewPassword: `${apiUser}passwordPolicies`,
    //userData
    userListOrg: `${apiUser}getData/listUserOrg`,
    //Roles
    listaRoles: `${apiUser}roles/listaRoles`,
    recaptcha: `${apiUser}recaptcha/v1/recaptchaVerificar`,
    //API PSE
    createTransaction: `${apiPse}createTransaction`,
    getTransaction: `${apiPse}getTransactions`,
    // API ORQUESTACION
    //restablecer contraseña
    recoveryPassword: `${apiOrquestacion}users/recoveryPassword/`,
    verificarTokenPassword: `${apiOrquestacion}security/searchToken/`,


    //API NEGOCIO
    insertarPago: `${apiNegocio}pagos/insertarPagos`,
    actualizarPago: `${apiNegocio}pagos/updAllPagos`,

//pagos
    iniciarVerificacionPago: `${apiOrquestacion}pagos/iniciarVerificacionPago`,
    listaDepartamentos: `${apiNegocio}listas/getDepartamento`,
    getMunicipios: `${apiNegocio}listas/getMunicipios/`,
    getFechaCorte: `${apiNegocio}liquidacion/getFechaCorte`,
    consultarDetallePagos: `${apiNegocio}pagos/consultarDetallePagos`,
    obtenerCooperativaByCod: `${apiNegocio}cooperativa/getEntidadByCode/`,
    getEntidades: `${apiNegocio}listas/getEntidades`,
    listaParametros: `${apiNegocio}listas/getListaDominios/`,
    listaDominios: `${apiNegocio}listas/consultarLista/`,
    deshabilitarParametro: `${apiNegocio}listas/deleteParameter/`,
    actualizarParametro: `${apiNegocio}listas/updateParameter`,
    agregarParametro: `${apiNegocio}listas/insertParameter`,
    autenticar: `${apiNegocio}authenticate`,
    getListParameter: `${apiNegocio}listas/getListParameter`,
    getDetalleLiquidacion: `${apiNegocio}liquidacion/getDetalleLiquidacion`,
    getPeriodo: `${apiNegocio}cooperativa/getPeriodoActual`,
    getBasesLiquidacion: `${apiNegocio}liquidacion/getBasesLiquidacion`,
    getTotalSaldoDepositos: `${apiNegocio}liquidacion/getTotalSaldoDepositos`,
    getDetalleRetransmision:`${apiNegocio}liquidacion/getDetalleRetransmision`,
    getSaldosPendientes: `${apiNegocio}liquidacion/getResumenSaldosPendientes`
};



